/*
 * Copyright (C) 2017 MINDORKS NEXTGEN PRIVATE LIMITED
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://mindorks.com/license/apache-v2
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */

package com.tomcat.agriculturerepository.data.network;

import com.google.gson.Gson;
import com.rx2androidnetworking.Rx2AndroidNetworking;
import com.tomcat.agriculturerepository.data.network.model.AddDataProdukResponse;
import com.tomcat.agriculturerepository.data.network.model.DataNamaProdukResponse;
import com.tomcat.agriculturerepository.data.network.model.DataProduk;
import com.tomcat.agriculturerepository.data.network.model.DataProdukByNamaRequest;
import com.tomcat.agriculturerepository.data.network.model.DataProdukByNamaResponse;
import com.tomcat.agriculturerepository.data.network.model.DeleteDataProdukRequest;
import com.tomcat.agriculturerepository.data.network.model.DeleteDataProdukResponse;
import com.tomcat.agriculturerepository.data.network.model.DetailDataProduk;
import com.tomcat.agriculturerepository.data.network.model.DetailDataProdukRequest;
import com.tomcat.agriculturerepository.data.network.model.DetailDataProdukResponse;
import com.tomcat.agriculturerepository.data.network.model.LoginRequest;
import com.tomcat.agriculturerepository.data.network.model.LoginResponse;
import com.tomcat.agriculturerepository.data.network.model.LogoutResponse;
import com.tomcat.agriculturerepository.data.network.model.UpdatePasswordRequest;
import com.tomcat.agriculturerepository.data.network.model.UpdatePasswordResponse;
import com.tomcat.agriculturerepository.data.network.model.UpdateProfileRequest;
import com.tomcat.agriculturerepository.data.network.model.UpdateProfileResponse;

import org.json.JSONException;
import org.json.JSONObject;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Observable;

/**
 * Created by janisharali on 28/01/17.
 */

@Singleton
public class ApiHelperApp implements ApiHelper {

    private ApiHeader mApiHeader;

    @Inject
    public ApiHelperApp(ApiHeader apiHeader) {
        mApiHeader = apiHeader;
    }

    @Override
    public ApiHeader getApiHeader() {
        return mApiHeader;
    }

    @Override
    public Observable<LoginResponse> doGoogleLoginApiCall(LoginRequest.GoogleLoginRequest
                                                                  request) {
        return Rx2AndroidNetworking.post(ApiEndPoint.ENDPOINT_GOOGLE_LOGIN)
                .addHeaders(mApiHeader.getPublicApiHeader())
                .addBodyParameter(request)
                .build()
                .getObjectObservable(LoginResponse.class);
    }

    @Override
    public Observable<LoginResponse> doFacebookLoginApiCall(LoginRequest.FacebookLoginRequest
                                                                    request) {
        return Rx2AndroidNetworking.post(ApiEndPoint.ENDPOINT_FACEBOOK_LOGIN)
                .addHeaders(mApiHeader.getPublicApiHeader())
                .addBodyParameter(request)
                .build()
                .getObjectObservable(LoginResponse.class);
    }

    @Override
    public Observable<LoginResponse> doServerLoginApiCall(LoginRequest.ServerLoginRequest
                                                                  request) {
        return Rx2AndroidNetworking.post(ApiEndPoint.ENDPOINT_SERVER_LOGIN)
                .addHeaders(mApiHeader.getPublicApiHeader())
                .addBodyParameter(request)
                .build()
                .getObjectObservable(LoginResponse.class);
    }

    @Override
    public Observable<UpdatePasswordResponse> doServerUpdatePasswordApiCall(UpdatePasswordRequest.ServerUpdatePasswordRequest request) {
        return Rx2AndroidNetworking.post(ApiEndPoint.ENDPOINT_UPDATE_PASSWORD)
                .addHeaders(mApiHeader.getPublicApiHeader())
                .addBodyParameter(request)
                .build()
                .getObjectObservable(UpdatePasswordResponse.class);
    }

    @Override
    public Observable<UpdateProfileResponse> doServerUpdateProfileApiCall(UpdateProfileRequest.ServerUpdateProfileRequest request) {
        return Rx2AndroidNetworking.post(ApiEndPoint.ENDPOINT_UPDATE_PROFILE)
                .addHeaders(mApiHeader.getPublicApiHeader())
                .addBodyParameter(request)
                .build()
                .getObjectObservable(UpdateProfileResponse.class);
    }

    @Override
    public Observable<LogoutResponse> doLogoutApiCall() {
        return Rx2AndroidNetworking.post(ApiEndPoint.ENDPOINT_LOGOUT)
                .addHeaders(mApiHeader.getProtectedApiHeader())
                .build()
                .getObjectObservable(LogoutResponse.class);
    }

    @Override
    public Observable<DataNamaProdukResponse> getDataNamaProdukApiCall() {
        return Rx2AndroidNetworking.get(ApiEndPoint.ENDPOINT_DATA_NAMA_PRODUK)
                .addHeaders(mApiHeader.getProtectedApiHeader())
                .build()
                .getObjectObservable(DataNamaProdukResponse.class);
    }

    @Override
    public Observable<DataProdukByNamaResponse> getDataProdukApiCall(DataProdukByNamaRequest
                                                                  request) {
        return Rx2AndroidNetworking.post(ApiEndPoint.ENDPOINT_DATA_PRODUK)
                .addHeaders(mApiHeader.getPublicApiHeader())
                .addApplicationJsonBody(request)
                .build()
                .getObjectObservable(DataProdukByNamaResponse.class);
    }

    @Override
    public Observable<DetailDataProdukResponse> getDetailDataProdukApiCall(DetailDataProdukRequest
                                                                             request) {
        return Rx2AndroidNetworking.post(ApiEndPoint.ENDPOINT_DETAIL_DATA_PRODUK)
                .addHeaders(mApiHeader.getPublicApiHeader())
                .addApplicationJsonBody(request)
                .build()
                .getObjectObservable(DetailDataProdukResponse.class);
    }

    @Override
    public Observable<AddDataProdukResponse> addDataProdukApiCall(DataProduk
                                                                                   request) {
        return Rx2AndroidNetworking.post(ApiEndPoint.ENDPOINT_ADD_DATA_PRODUK)
                .addHeaders(mApiHeader.getPublicApiHeader())
                .addApplicationJsonBody(request)
                .build()
                .getObjectObservable(AddDataProdukResponse.class);
    }

    @Override
    public Observable<DeleteDataProdukResponse> deleteDataProdukApiCall(DeleteDataProdukRequest
                                                                          request) {
        return Rx2AndroidNetworking.post(ApiEndPoint.ENDPOINT_DELETE_DATA_PRODUK)
                .addHeaders(mApiHeader.getPublicApiHeader())
                .addApplicationJsonBody(request)
                .build()
                .getObjectObservable(DeleteDataProdukResponse.class);
    }
}

