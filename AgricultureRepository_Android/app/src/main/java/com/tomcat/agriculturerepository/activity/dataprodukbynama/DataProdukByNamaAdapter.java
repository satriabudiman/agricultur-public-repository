/*
 * Copyright (C) 2017 MINDORKS NEXTGEN PRIVATE LIMITED
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://mindorks.com/license/apache-v2
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */

package com.tomcat.agriculturerepository.activity.dataprodukbynama;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.tomcat.agriculturerepository.R;
import com.tomcat.agriculturerepository.activity.base.BaseViewHolder;
import com.tomcat.agriculturerepository.data.network.model.DataProdukByNama;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Janisharali on 25-05-2017.
 */

public class DataProdukByNamaAdapter extends RecyclerView.Adapter<BaseViewHolder> {

    public static final int VIEW_TYPE_EMPTY = 0;
    public static final int VIEW_TYPE_NORMAL = 1;

    private Callback mCallback;
    private List<DataProdukByNama> mDataProdukByNamaResponseList;

    public DataProdukByNamaAdapter(List<DataProdukByNama> dataProdukByNamaList) {
        mDataProdukByNamaResponseList = dataProdukByNamaList;
    }

    public void setCallback(Callback callback) {
        mCallback = callback;
    }

    @Override
    public void onBindViewHolder(BaseViewHolder holder, int position) {
        holder.onBind(position);
    }

    @Override
    public BaseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        switch (viewType) {
            case VIEW_TYPE_NORMAL:
                return new ViewHolder(
                        LayoutInflater.from(parent.getContext()).inflate(R.layout.item_data_produk_by_nama_view, parent, false));
            case VIEW_TYPE_EMPTY:
            default:
                return new EmptyViewHolder(
                        LayoutInflater.from(parent.getContext()).inflate(R.layout.item_empty_view, parent, false));
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (mDataProdukByNamaResponseList != null && mDataProdukByNamaResponseList.size() > 0) {
            return VIEW_TYPE_NORMAL;
        } else {
            return VIEW_TYPE_EMPTY;
        }
    }

    @Override
    public int getItemCount() {
        if (mDataProdukByNamaResponseList != null && mDataProdukByNamaResponseList.size() > 0) {
            return mDataProdukByNamaResponseList.size();
        } else {
            return 1;
        }
    }

    public void addItems(List<DataProdukByNama> dataProdukByNamaList) {
        mDataProdukByNamaResponseList.addAll(dataProdukByNamaList);
        notifyDataSetChanged();
    }

    public interface Callback {
        void onEmptyViewRetryClick();
        void onItemClick(DataProdukByNama dataProdukByNama);
    }

    public class ViewHolder extends BaseViewHolder {

        @BindView(R.id.status_berhasil_text_view)
        TextView statusBerhasilTextView;

        @BindView(R.id.persen_untung_text_view)
        TextView persenKeuntunganTextView;

        @BindView(R.id.jumlah_vote_text_view)
        TextView jumlahVoteTextView;

        @BindView(R.id.lokasi_text_view)
        TextView lokasiTextView;

        @BindView(R.id.jumlah_penyakit_text_view)
        TextView jumlahPenyakitTextView;

        @BindView(R.id.tanggal_tanam_text_view)
        TextView tanggalTanamTextView;

        @BindView(R.id.tanggal_panen_text_view)
        TextView tanggalPanenTextView;


        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        protected void clear() {
            persenKeuntunganTextView.setText("");
        }

        public void onBind(int position) {
            super.onBind(position);

            final DataProdukByNama dataProdukByNama = mDataProdukByNamaResponseList.get(position);

            if (dataProdukByNama.getStatusBerhasil()!=null) {
                if(dataProdukByNama.getStatusBerhasil()) {
                    statusBerhasilTextView.setText(R.string.text_status_berhasil);
                } else {
                    statusBerhasilTextView.setText(R.string.text_status_gagal);
                }
            }

            if (dataProdukByNama.getPersenUntung() != null) {
                String untung = String.valueOf(dataProdukByNama.getPersenUntung());
                if(dataProdukByNama.getPersenUntung()>=0) {
                    untung += " % Untung";
                } else {
                    untung += " % Rugi";
                }
                persenKeuntunganTextView.setText(untung);
            }

            if (dataProdukByNama.getJumlahVote() != null) {
                jumlahVoteTextView.setText(String.valueOf(dataProdukByNama.getJumlahVote()).concat(" Vote"));
            }

            if (dataProdukByNama.getJumlahPenyakit() != null) {
                jumlahPenyakitTextView.setText(String.valueOf(dataProdukByNama.getJumlahPenyakit()).concat(" Penyakit"));
            }

            if (dataProdukByNama.getLokasi() != null) {
                lokasiTextView.setText(dataProdukByNama.getLokasi());
            }

            if (dataProdukByNama.getTanggalTanam() != null) {
                String viewTanggalTanam = "Tanggal Tanam "+dataProdukByNama.getTanggalTanam();
                tanggalTanamTextView.setText(viewTanggalTanam);
            }

            if (dataProdukByNama.getTanggalPanen() != null) {
                String viewTanggalPanen = "Tanggal Panen "+dataProdukByNama.getTanggalTanam();
                tanggalPanenTextView.setText(viewTanggalPanen);
            }

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (dataProdukByNama.getId() != null) {
                        mCallback.onItemClick(dataProdukByNama);
                    }
                }
            });
        }
    }

    public class EmptyViewHolder extends BaseViewHolder {

        @BindView(R.id.btn_retry)
        Button retryButton;

        @BindView(R.id.tv_message)
        TextView messageTextView;

        public EmptyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @Override
        protected void clear() {

        }

        @OnClick(R.id.btn_retry)
        void onRetryClick() {
            if (mCallback != null)
                mCallback.onEmptyViewRetryClick();
        }
    }
}
