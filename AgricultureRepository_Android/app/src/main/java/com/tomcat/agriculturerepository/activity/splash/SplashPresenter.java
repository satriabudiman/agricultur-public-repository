package com.tomcat.agriculturerepository.activity.splash;

import com.tomcat.agriculturerepository.activity.base.BasePresenter;
import com.tomcat.agriculturerepository.di.PerActivity;

/**
 * Created by albertbrucelee on 21/12/17.
 */

@PerActivity
public interface SplashPresenter<V extends SplashView> extends BasePresenter<V> {

}
