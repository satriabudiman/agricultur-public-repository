package com.tomcat.agriculturerepository.data.network.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by albertbrucelee on 26/12/17.
 */

public class ProdukPupuk {

    @Expose
    @SerializedName("id")
    private String id;

    @Expose
    @SerializedName("name")
    private String namaPupuk;

    @Expose
    @SerializedName("harga")
    private String harga;

    @Expose
    @SerializedName("satuan")
    private String satuanHarga;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setNamaPupuk(String namaPupuk) {
        this.namaPupuk = namaPupuk;
    }

    public String getNamaPupuk() {
        return namaPupuk;
    }

    public String getHarga() {
        return harga;
    }

    public void setHarga(String harga) {
        this.harga = harga;
    }

    public String getSatuanHarga() {
        return satuanHarga;
    }

    public void setSatuanHarga(String satuanHarga) {
        this.satuanHarga = satuanHarga;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ProdukPupuk)) return false;

        ProdukPupuk produkPupuk = (ProdukPupuk) o;

        if (!id.equals(produkPupuk.id)) return false;
        if (!namaPupuk.equals(produkPupuk.namaPupuk)) return false;
        if (!harga.equals(produkPupuk.harga)) return false;
        return satuanHarga.equals(produkPupuk.satuanHarga);
    }

    @Override
    public int hashCode() {
        int result = id.hashCode();
        result = 31 * result + namaPupuk.hashCode();
        result = 31 * result + harga.hashCode();
        result = 31 * result + satuanHarga.hashCode();
        return result;
    }
}