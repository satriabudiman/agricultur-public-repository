package com.tomcat.agriculturerepository.data.network.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by albertbrucelee on 26/12/17.
 */


public class DataProduk {
    @Expose
    @SerializedName("id")
    protected Integer id;


    @Expose
    @SerializedName("status")
    protected Boolean statusBerhasil;

    @Expose
    @SerializedName("idPemilik")
    protected String idPemilik;

    @Expose
    @SerializedName("namaPemilik")
    protected String namaPemilik;

    @Expose
    @SerializedName("tanggalTanam")
    protected String tanggalTanam;

    @Expose
    @SerializedName("tanggalPanen")
    protected String tanggalPanen;

    @Expose
    @SerializedName("luasLahan")
    protected Float luasLahan;

    @Expose
    @SerializedName("lokasi")
    protected String lokasi;

    @Expose
    @SerializedName("suhu")
    protected String suhu;

    @Expose
    @SerializedName("modal")
    protected Float hargaBibit;

    @Expose
    @SerializedName("satuanHargaModal")
    protected SatuanHargaBibit satuanHargaBibit;

    @Expose
    @SerializedName("harga")
    protected Float hargaJual;

    @Expose
    @SerializedName("satuanHargaJual")
    protected SatuanHargaJual satuanHargaJual;

    @Expose
    @SerializedName("photos")
    protected List<ProdukFoto> produkFotoList;

    @Expose
    @SerializedName("diseases")
    protected List<ProdukPenyakit> produkPenyakitList;

    @Expose
    @SerializedName("pupuk")
    protected List<ProdukPupuk> produkPupukList;

    public Integer getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

//    public KategoriNamaProduk getKategoriNamaProduk() {
//        return kategoriNamaProduk;
//    }
//
//    public void setKategoriNamaProduk(KategoriNamaProduk kategoriNamaProduk) {
//        this.kategoriNamaProduk = kategoriNamaProduk;
//    }

    public Boolean getStatusBerhasil() {
        return statusBerhasil;
    }

    public void setStatusBerhasil(boolean statusBerhasil) {
        this.statusBerhasil = statusBerhasil;
    }

    public String getIdPemilik() {
        return idPemilik;
    }

    public void setIdPemilik(String idPemilik) {
        this.idPemilik = idPemilik;
    }

    public String getNamaPemilik() {
        return namaPemilik;
    }

    public void setNamaPemilik(String namaPemilik) {
        this.namaPemilik = namaPemilik;
    }

    public String getTanggalTanam() {
        return tanggalTanam;
    }

    public void setTanggalTanam(String tanggalTanam) {
        this.tanggalTanam = tanggalTanam;
    }

    public String getTanggalPanen() {
        return tanggalPanen;
    }

    public void setTanggalPanen(String tanggalPanen) {
        this.tanggalPanen = tanggalPanen;
    }

    public Float getLuasLahan() {
        return luasLahan;
    }

    public void setLuasLahan(float luasLahan) {
        this.luasLahan = luasLahan;
    }

    public String getLokasi() {
        return lokasi;
    }

    public void setLokasi(String lokasi) {
        this.lokasi = lokasi;
    }

    public String getSuhu() {
        return suhu;
    }

    public void setSuhu(String suhu) {
        this.suhu = suhu;
    }

    public Float getHargaBibit() {
        return hargaBibit;
    }

    public void setHargaBibit(float hargaBibit) {
        this.hargaBibit = hargaBibit;
    }

    public SatuanHargaBibit getSatuanHargaBibit() {
        return satuanHargaBibit;
    }

    public void setSatuanHargaBibit(SatuanHargaBibit satuanHargaBibit) {
        this.satuanHargaBibit = satuanHargaBibit;
    }

    public Float getHargaJual() {
        return hargaJual;
    }

    public void setHargaJual(float hargaJual) {
        this.hargaJual = hargaJual;
    }

    public SatuanHargaJual getSatuanHargaJual() {
        return satuanHargaJual;
    }

    public void setSatuanHargaJual(SatuanHargaJual satuanHargaJual) {
        this.satuanHargaJual = satuanHargaJual;
    }

    public List<ProdukFoto> getProdukFotoList() {
        return produkFotoList;
    }

    public void setProdukFotoList(List<ProdukFoto> produkFotoList) {
        this.produkFotoList = produkFotoList;
    }

    public List<ProdukPenyakit> getProdukPenyakitList() {
        return produkPenyakitList;
    }

    public void setProdukPenyakitList(List<ProdukPenyakit> produkPenyakitList) {
        this.produkPenyakitList = produkPenyakitList;
    }

    public List<ProdukPupuk> getProdukPupukList() {
        return produkPupukList;
    }

    public void setProdukPupukList(List<ProdukPupuk> produkPupukList) {
        this.produkPupukList = produkPupukList;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof DataProduk)) return false;

        DataProduk detailDataProduk = (DataProduk) o;

        if (!id.equals(detailDataProduk.id)) return false;
        if (!statusBerhasil==detailDataProduk.statusBerhasil) return false;
        if (!idPemilik.equals(detailDataProduk.idPemilik)) return false;
        if (!namaPemilik.equals(detailDataProduk.namaPemilik)) return false;
        if (!tanggalTanam.equals(detailDataProduk.tanggalTanam)) return false;
        if (!tanggalPanen.equals(detailDataProduk.tanggalPanen)) return false;
        if (!lokasi.equals(detailDataProduk.lokasi)) return false;
        if (!suhu.equals(detailDataProduk.suhu)) return false;
        if (!hargaBibit.equals(detailDataProduk.hargaBibit)) return false;
        if (!satuanHargaBibit.equals(detailDataProduk.satuanHargaBibit)) return false;
        if (!hargaJual.equals(detailDataProduk.hargaJual)) return false;
        if (!satuanHargaJual.equals(detailDataProduk.satuanHargaJual)) return false;
        if (!produkFotoList.equals(detailDataProduk.produkFotoList)) return false;
        if (!produkPenyakitList.equals(detailDataProduk.produkPenyakitList)) return false;
        return produkPupukList.equals(detailDataProduk.produkPupukList);
    }

    @Override
    public int hashCode() {
        int result = id.hashCode();
        result = 31 * result + statusBerhasil.hashCode();
        result = 31 * result + idPemilik.hashCode();
        result = 31 * result + namaPemilik.hashCode();
        result = 31 * result + tanggalTanam.hashCode();
        result = 31 * result + tanggalPanen.hashCode();
        result = 31 * result + lokasi.hashCode();
        result = 31 * result + suhu.hashCode();
        result = 31 * result + hargaBibit.hashCode();
        result = 31 * result + satuanHargaBibit.hashCode();
        result = 31 * result + hargaJual.hashCode();
        result = 31 * result + satuanHargaJual.hashCode();
        result = 31 * result + produkFotoList.hashCode();
        result = 31 * result + produkPenyakitList.hashCode();
        result = 31 * result + produkPupukList.hashCode();
        return result;
    }
}