package com.tomcat.agriculturerepository.activity.base;

import android.support.annotation.StringRes;

/**
 * Created by albertbrucelee on 21/12/17.
 */

public interface BaseView {

    void showLoading();

    void hideLoading();

    void openActivityOnTokenExpire();

    void onError(@StringRes int resId);

    void onError(String message);

    void showMessage(String message);

    void showMessage(@StringRes int resId);

    boolean isNetworkConnected();

    void hideKeyboard();
}
