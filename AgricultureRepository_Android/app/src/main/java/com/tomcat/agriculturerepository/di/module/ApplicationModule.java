/*
 * Copyright (C) 2017 MINDORKS NEXTGEN PRIVATE LIMITED
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://mindorks.com/license/apache-v2
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */

package com.tomcat.agriculturerepository.di.module;

import android.app.Application;
import android.content.Context;


import com.tomcat.agriculturerepository.BuildConfig;
import com.tomcat.agriculturerepository.R;
import com.tomcat.agriculturerepository.data.DataManager;
import com.tomcat.agriculturerepository.data.DataManagerApp;
import com.tomcat.agriculturerepository.data.db.DbHelperApp;
import com.tomcat.agriculturerepository.data.db.DbHelper;
import com.tomcat.agriculturerepository.data.network.ApiHeader;
import com.tomcat.agriculturerepository.data.network.ApiHelper;
import com.tomcat.agriculturerepository.data.network.ApiHelperApp;
import com.tomcat.agriculturerepository.data.prefs.AppPreferencesHelper;
import com.tomcat.agriculturerepository.data.prefs.PreferencesHelper;
import com.tomcat.agriculturerepository.di.ApiInfo;
import com.tomcat.agriculturerepository.di.ApplicationContext;
import com.tomcat.agriculturerepository.di.DatabaseInfo;
import com.tomcat.agriculturerepository.di.PreferenceInfo;
import com.tomcat.agriculturerepository.utils.AppConstants;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

/**
 * Created by janisharali on 27/01/17.
 */

@Module
public class ApplicationModule {

    private final Application mApplication;

    public ApplicationModule(Application application) {
        mApplication = application;
    }

    @Provides
    @ApplicationContext
    Context provideContext() {
        return mApplication;
    }

    @Provides
    Application provideApplication() {
        return mApplication;
    }

    @Provides
    @DatabaseInfo
    String provideDatabaseName() {
        return AppConstants.DB_NAME;
    }

    @Provides
    @ApiInfo
    String provideApiKey() {
        return BuildConfig.API_KEY;
    }

    @Provides
    @PreferenceInfo
    String providePreferenceName() {
        return AppConstants.PREF_NAME;
    }

    @Provides
    @Singleton
    DataManager provideDataManager(DataManagerApp appDataManager) {
        return appDataManager;
    }

    @Provides
    @Singleton
    DbHelper provideDbHelper(DbHelperApp dbHelperApp) {
        return dbHelperApp;
    }

    @Provides
    @Singleton
    PreferencesHelper providePreferencesHelper(AppPreferencesHelper appPreferencesHelper) {
        return appPreferencesHelper;
    }

    @Provides
    @Singleton
    ApiHelper provideApiHelper(ApiHelperApp apiHelperApp) {
        return apiHelperApp;
    }

    @Provides
    @Singleton
    ApiHeader.ProtectedApiHeader provideProtectedApiHeader(@ApiInfo String apiKey,
                                                           PreferencesHelper preferencesHelper) {
        return new ApiHeader.ProtectedApiHeader(
                apiKey,
                preferencesHelper.getCurrentUserId(),
                preferencesHelper.getAccessToken());
    }

    @Provides
    @Singleton
    CalligraphyConfig provideCalligraphyDefaultConfig() {
        return new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/source-sans-pro/SourceSansPro-Regular.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build();
    }
}
