/*
 * Copyright (C) 2017 MINDORKS NEXTGEN PRIVATE LIMITED
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://mindorks.com/license/apache-v2
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */

package com.tomcat.agriculturerepository.data.network;

import com.tomcat.agriculturerepository.BuildConfig;

/**
 * Created by amitshekhar on 01/02/17.
 */

public final class ApiEndPoint {

    public static final String ENDPOINT_GOOGLE_LOGIN = BuildConfig.BASE_URL
            + "/588d14f4100000a9072d2943";

    public static final String ENDPOINT_FACEBOOK_LOGIN = BuildConfig.BASE_URL
            + "/588d15d3100000ae072d2944";

    public static final String ENDPOINT_SERVER_LOGIN = "http://192.168.1.3/agriculturerepo/index.php" //BuildConfig.BASE_URL
            + "/oauth/token";

    public static final String ENDPOINT_LOGOUT = "http://192.168.1.3/agriculturerepo/index.php" //BuildConfig.BASE_URL
            + "/588d161c100000a9072d2946";

    public static final String ENDPOINT_DATA_NAMA_PRODUK = BuildConfig.BASE_URL //"http://172.20.33.42:8081/api/v1"
            + "/category/summary";

    public static final String ENDPOINT_DATA_PRODUK = BuildConfig.BASE_URL  //"http://192.168.1.167/agriculturerepo/index.php
            + "/category/product";

    public static final String ENDPOINT_DETAIL_DATA_PRODUK = BuildConfig.BASE_URL //"http://172.20.33.42:8081/api/v1"
            + "/agriculturalproduct/detail";

    public static final String ENDPOINT_ADD_DATA_PRODUK = BuildConfig.BASE_URL
            + "/agriculturalproduct";
//    public static final String ENDPOINT_ADD_DATA_PRODUK = "http://172.20.33.42:8081/api/v1"
//            + "/agriculturalproduct";

    public static final String ENDPOINT_DELETE_DATA_PRODUK = BuildConfig.BASE_URL
            + "/5926c34212000035026871ci";

    public static final String ENDPOINT_UPDATE_PASSWORD = BuildConfig.BASE_URL
            + "/5926c34212000087364535de";

    public static final String ENDPOINT_UPDATE_PROFILE = BuildConfig.BASE_URL
            + "/5926c34212000087364535df";

    private ApiEndPoint() {
        // This class is not publicly instantiable
    }

}
