package com.tomcat.agriculturerepository.activity.login;

import com.tomcat.agriculturerepository.activity.base.BasePresenter;
import com.tomcat.agriculturerepository.di.PerActivity;

/**
 * Created by albertbrucelee on 21/12/17.
 */

@PerActivity
public interface LoginPresenter<V extends LoginView> extends BasePresenter<V> {

    void validateCredentials(String username, String password);

//    void onUsernameError();
//
//    void onPasswordError();
//
//    void onSuccess();

}