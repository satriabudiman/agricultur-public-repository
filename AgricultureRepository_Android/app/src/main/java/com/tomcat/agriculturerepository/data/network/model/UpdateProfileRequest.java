package com.tomcat.agriculturerepository.data.network.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Michael on 27/12/2017.
 */

public class UpdateProfileRequest {
    private UpdateProfileRequest() {
        // This class is not publicly instantiable
    }

    public static class ServerUpdateProfileRequest {
        @Expose
        @SerializedName("id")
        private Long id;

        @Expose
        @SerializedName("name")
        private String name;

        @Expose
        @SerializedName("jenis_kelamin")
        private String jenisKelamin;

        @Expose
        @SerializedName("tanggal_lahir")
        private String tanggalLahir;

        @Expose
        @SerializedName("email")
        private String email;

        @Expose
        @SerializedName("profile_photo")
        private String profilePhoto;

        public ServerUpdateProfileRequest(Long id, String name, String jenisKelamin, String tanggalLahir, String email, String profilePhoto) {
            this.id = id;
            this.name = name;
            this.jenisKelamin = jenisKelamin;
            this.tanggalLahir = tanggalLahir;
            this.email = email;
            this.profilePhoto = profilePhoto;
        }

        public Long getId() {
            return id;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getName() {
            return name;
        }

        public void setJenisKelamin(String jenisKelamin) {
            this.jenisKelamin = jenisKelamin;
        }

        public String getJenisKelamin() {
            return jenisKelamin;
        }

        public void setTanggalLahir(String tanggalLahir) {
            this.tanggalLahir = tanggalLahir;
        }

        public String getTanggalLahir() {
            return tanggalLahir;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getEmail() {
            return email;
        }

        public String getProfilePhoto() {
            return profilePhoto;
        }

        @Override
        public boolean equals(Object object) {
            if (this == object) return true;
            if (object == null || getClass() != object.getClass()) return false;

            UpdateProfileRequest.ServerUpdateProfileRequest that = (UpdateProfileRequest.ServerUpdateProfileRequest) object;

            if (id != null ? !id.equals(that.id) : that.id != null) return false;
            return (name != null ? name.equals(that.name) : that.name == null) && (jenisKelamin != null ? jenisKelamin.equals(that.jenisKelamin) : that.jenisKelamin == null) && (tanggalLahir != null ? tanggalLahir.equals(that.tanggalLahir) : that.tanggalLahir == null) && (email != null ? email.equals(that.email) : that.email == null) && (profilePhoto != null ? profilePhoto.equals(that.profilePhoto) : that.profilePhoto == null);

        }

        @Override
        public int hashCode() {
            int result = id != null ? id.hashCode() : 0;
            result = 31 * result + (name != null ? name.hashCode() : 0);
            result = 31 * result + (jenisKelamin != null ? jenisKelamin.hashCode() : 0);
            result = 31 * result + (tanggalLahir != null ? tanggalLahir.hashCode() : 0);
            result = 31 * result + (email != null ? email.hashCode() : 0);
            result = 31 * result + (profilePhoto != null ? profilePhoto.hashCode() : 0);
            return result;
        }
    }
}
