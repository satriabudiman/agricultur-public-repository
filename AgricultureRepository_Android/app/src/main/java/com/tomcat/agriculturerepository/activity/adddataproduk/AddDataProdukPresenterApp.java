/*
 * Copyright (C) 2017 MINDORKS NEXTGEN PRIVATE LIMITED
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://mindorks.com/license/apache-v2
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */

package com.tomcat.agriculturerepository.activity.adddataproduk;

import com.androidnetworking.error.ANError;
import com.tomcat.agriculturerepository.R;
import com.tomcat.agriculturerepository.activity.base.BasePresenterApp;
import com.tomcat.agriculturerepository.data.DataManager;
import com.tomcat.agriculturerepository.data.network.model.AddDataProduk;
import com.tomcat.agriculturerepository.data.network.model.AddDataProdukResponse;
import com.tomcat.agriculturerepository.data.network.model.DataProduk;
import com.tomcat.agriculturerepository.data.network.model.KategoriNamaProduk;
import com.tomcat.agriculturerepository.data.network.model.ProdukFoto;
import com.tomcat.agriculturerepository.data.network.model.ProdukPenyakit;
import com.tomcat.agriculturerepository.data.network.model.ProdukPupuk;
import com.tomcat.agriculturerepository.data.network.model.SatuanHargaBibit;
import com.tomcat.agriculturerepository.data.network.model.SatuanHargaJual;
import com.tomcat.agriculturerepository.utils.AppConstants;
import com.tomcat.agriculturerepository.utils.rx.SchedulerProvider;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;

/**
 * Created by janisharali on 25/05/17.
 */

public class AddDataProdukPresenterApp<V extends AddDataProdukView> extends BasePresenterApp<V>
        implements AddDataProdukPresenter<V> {

    @Inject
    public AddDataProdukPresenterApp(DataManager dataManager,
                                        SchedulerProvider schedulerProvider,
                                        CompositeDisposable compositeDisposable) {
        super(dataManager, schedulerProvider, compositeDisposable);
    }

    public void sendAddProduk(
                                boolean statusBerhasil
                                , String idNamaProduk
                                , String tanggalTanam
                                , String tanggalPanen
                                , String luasLahan
                                , String lokasi
                                , String suhu
                                , String hargaBibit
                                , String idSatuanHargaBibit
                                , String hargaJual
                                , String idSatuanHargaJual
                                , String linkFoto
                                , String idPenyakit
                                , String idPupuk
                        ) {

        if (idNamaProduk == null || idNamaProduk.isEmpty()) {
            getView().onError("Harap Isi Nama Produk");
            return;
        }
        if (tanggalTanam == null || tanggalTanam.isEmpty()) {
            getView().onError("Harap Isi Tanggal Tanam");
            return;
        }
        if (tanggalPanen == null || tanggalPanen.isEmpty()) {
            getView().onError("Harap Isi Tanggal Panen");
            return;
        }
        if (luasLahan == null || luasLahan.isEmpty()) {
            getView().onError("Harap Isi Luas Lahan");
            return;
        }
        if (lokasi == null || lokasi.isEmpty()) {
            getView().onError("Harap Isi Lokasi");
            return;
        }
        if (suhu == null || suhu.isEmpty()) {
            getView().onError("Harap Isi Suhu");
            return;
        }
        if (hargaBibit == null || hargaBibit.isEmpty()) {
            getView().onError("Harap Isi Harga Bibit");
            return;
        }
        if (idSatuanHargaBibit == null || idSatuanHargaBibit.isEmpty()) {
            getView().onError("Harap Isi Satuan Harga Bibit");
            return;
        }
        if (hargaJual == null || hargaJual.isEmpty()) {
            getView().onError("Harap Isi Harga Jual");
            return;
        }
        if (idSatuanHargaJual == null || idSatuanHargaJual.isEmpty()) {
            getView().onError("Harap Isi Satuan Harga Jual");
            return;
        }
        if (linkFoto == null || linkFoto.isEmpty()) {
            getView().onError("Harap Isi Link Foto");
            return;
        }
        if (idPenyakit == null || idPenyakit.isEmpty()) {
            getView().onError("Harap Isi Penyakit");
            return;
        }
        if (idPupuk == null || idPupuk.isEmpty()) {
            getView().onError("Harap Isi Pupuk");
            return;
        }

        ProdukFoto produkFoto = new ProdukFoto();
        produkFoto.setUrl(linkFoto);
        List<ProdukFoto> produkFotoList = new ArrayList<>();
        produkFotoList.add(produkFoto);

        ProdukPenyakit produkPenyakit = new ProdukPenyakit();
        produkPenyakit.setId(idPenyakit);
        List<ProdukPenyakit> produkPenyakitList = new ArrayList<>();
        produkPenyakitList.add(produkPenyakit);

        ProdukPupuk produkPupuk = new ProdukPupuk();
        produkPupuk.setId(idPupuk);
        List<ProdukPupuk> produkPupukList = new ArrayList<>();
        produkPupukList.add(produkPupuk);

        KategoriNamaProduk kategoriProduk = new KategoriNamaProduk();
        kategoriProduk.setId(Integer.parseInt(idNamaProduk));

        SatuanHargaBibit satuanHargaBibit = new SatuanHargaBibit();
        satuanHargaBibit.setId(Integer.parseInt(idSatuanHargaBibit));

        SatuanHargaJual satuanHargaJual = new SatuanHargaJual();
        satuanHargaJual.setId(Integer.parseInt(idSatuanHargaJual));

        AddDataProduk dataProduk = new AddDataProduk();
        dataProduk.setStatusBerhasil(statusBerhasil);
        dataProduk.setKategoriNamaProduk(kategoriProduk);
        dataProduk.setTanggalTanam(tanggalTanam);
        dataProduk.setTanggalPanen(tanggalPanen);
        dataProduk.setLuasLahan(Float.parseFloat(luasLahan));
        dataProduk.setLokasi(lokasi);
        dataProduk.setSuhu(suhu);
        dataProduk.setHargaBibit(Float.parseFloat(hargaBibit));
        dataProduk.setSatuanHargaBibit(satuanHargaBibit);
        dataProduk.setHargaJual(Float.parseFloat(hargaJual));
        dataProduk.setSatuanHargaJual(satuanHargaJual);
        dataProduk.setProdukFotoList(produkFotoList);
        dataProduk.setProdukPenyakitList(produkPenyakitList);
        dataProduk.setProdukPupukList(produkPupukList);

        getView().showLoading();
        getCompositeDisposable().add(getDataManager()
                .addDataProdukApiCall(dataProduk)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(new Consumer<AddDataProdukResponse>() {
                    @Override
                    public void accept(AddDataProdukResponse response) throws Exception {
                        if (!isViewAttached()) {
                            return;
                        }

                        getView().hideLoading();
                        if(response.getStatusCode().equals(AppConstants.STATUS_CODE_SUCCESS)) {
                            getView().showMessage(R.string.message_success_add_data_produk);
                            getView().navigateDoneAddActivity();
                        } else {
                            getView().showMessage(response.getMessage());
                        }

                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {

                        if (!isViewAttached()) {
                            return;
                        }

                        getView().hideLoading();

                        // handle the login error here
                        if (throwable instanceof ANError) {
                            ANError anError = (ANError) throwable;
                            handleApiError(anError);
                        }
                    }
                }));
    }

}
