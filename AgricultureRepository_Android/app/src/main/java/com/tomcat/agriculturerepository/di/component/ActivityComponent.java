/*
 * Copyright (C) 2017 MINDORKS NEXTGEN PRIVATE LIMITED
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://mindorks.com/license/apache-v2
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */

package com.tomcat.agriculturerepository.di.component;

import com.tomcat.agriculturerepository.activity.about.AboutFragment;
import com.tomcat.agriculturerepository.activity.adddataproduk.AddDataProdukFragment;
import com.tomcat.agriculturerepository.activity.dataprodukbynama.DataProdukByNamaByNamaFragment;
import com.tomcat.agriculturerepository.activity.detaildataproduk.DetailDataProdukFragment;
import com.tomcat.agriculturerepository.activity.updatepassword.ChangePasswordActivity;
import com.tomcat.agriculturerepository.activity.updateprofile.ChangeProfileActivity;
import com.tomcat.agriculturerepository.di.PerActivity;
import com.tomcat.agriculturerepository.activity.login.LoginActivity;
import com.tomcat.agriculturerepository.activity.main.MainActivity;
import com.tomcat.agriculturerepository.activity.splash.SplashActivity;
import com.tomcat.agriculturerepository.di.module.ActivityModule;

import dagger.Component;

/**
 * Created by janisharali on 27/01/17.
 */

@PerActivity
@Component(dependencies = ApplicationComponent.class, modules = ActivityModule.class)
public interface ActivityComponent {

    void inject(MainActivity activity);

    void inject(LoginActivity activity);

    void inject(SplashActivity activity);

    void inject(AboutFragment fragment);

    void inject(DataProdukByNamaByNamaFragment fragment);

    void inject(DetailDataProdukFragment fragment);

    void inject(AddDataProdukFragment activity);

    void inject(ChangePasswordActivity activity);

    void inject(ChangeProfileActivity activity);
}
