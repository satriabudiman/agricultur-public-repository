/*
 * Copyright (C) 2017 MINDORKS NEXTGEN PRIVATE LIMITED
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://mindorks.com/license/apache-v2
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */

package com.tomcat.agriculturerepository.activity.dataprodukbynama;

import com.androidnetworking.error.ANError;
import com.tomcat.agriculturerepository.activity.base.BasePresenterApp;
import com.tomcat.agriculturerepository.data.DataManager;
import com.tomcat.agriculturerepository.data.network.model.DataProdukByNamaRequest;
import com.tomcat.agriculturerepository.data.network.model.DataProdukByNamaResponse;
import com.tomcat.agriculturerepository.utils.rx.SchedulerProvider;

import javax.inject.Inject;

import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;

/**
 * Created by janisharali on 25/05/17.
 */

public class DataProdukByNamaPresenterApp<V extends DataProdukByNamaView> extends BasePresenterApp<V>
        implements DataProdukByNamaPresenter<V> {

    public String idNamaProduk;

    @Inject
    public DataProdukByNamaPresenterApp(DataManager dataManager,
                                        SchedulerProvider schedulerProvider,
                                        CompositeDisposable compositeDisposable) {
        super(dataManager, schedulerProvider, compositeDisposable);
    }

    @Override
    public void setIdNamaProduk(String idNamaProduk){
        this.idNamaProduk = idNamaProduk;
    }

    @Override
    public void onViewPrepared() {
        getView().showLoading();
        getCompositeDisposable().add(getDataManager()
                .getDataProdukApiCall(new DataProdukByNamaRequest(idNamaProduk))
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(new Consumer<DataProdukByNamaResponse>() {
                    @Override
                    public void accept(@NonNull DataProdukByNamaResponse dataProdukByNamaResponse)
                            throws Exception {
                            if (dataProdukByNamaResponse != null && dataProdukByNamaResponse.getData() != null) {
                            getView().updateDataProduk(dataProdukByNamaResponse.getData());
                        }
                        getView().hideLoading();
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(@NonNull Throwable throwable)
                            throws Exception {
                        if (!isViewAttached()) {
                            return;
                        }

                        getView().hideLoading();

                        // handle the error here
                        if (throwable instanceof ANError) {
                            ANError anError = (ANError) throwable;
                            handleApiError(anError);
                        }
                    }
                }));
    }
}
