/*
 * Copyright (C) 2017 MINDORKS NEXTGEN PRIVATE LIMITED
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://mindorks.com/license/apache-v2
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */

package com.tomcat.agriculturerepository.di.module;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;


import com.tomcat.agriculturerepository.activity.about.AboutPresenter;
import com.tomcat.agriculturerepository.activity.about.AboutView;
import com.tomcat.agriculturerepository.activity.about.AboutPresenterApp;
import com.tomcat.agriculturerepository.activity.adddataproduk.AddDataProdukPresenter;
import com.tomcat.agriculturerepository.activity.adddataproduk.AddDataProdukPresenterApp;
import com.tomcat.agriculturerepository.activity.adddataproduk.AddDataProdukView;
import com.tomcat.agriculturerepository.activity.dataprodukbynama.DataProdukByNamaAdapter;
import com.tomcat.agriculturerepository.activity.dataprodukbynama.DataProdukByNamaPresenter;
import com.tomcat.agriculturerepository.activity.dataprodukbynama.DataProdukByNamaPresenterApp;
import com.tomcat.agriculturerepository.activity.dataprodukbynama.DataProdukByNamaView;
import com.tomcat.agriculturerepository.activity.detaildataproduk.DetailDataProdukPresenter;
import com.tomcat.agriculturerepository.activity.detaildataproduk.DetailDataProdukPresenterApp;
import com.tomcat.agriculturerepository.activity.detaildataproduk.DetailDataProdukView;
import com.tomcat.agriculturerepository.activity.detaildataproduk.KomentarProdukAdapter;
import com.tomcat.agriculturerepository.activity.detaildataproduk.PhotoProdukAdapter;
import com.tomcat.agriculturerepository.activity.login.LoginPresenter;
import com.tomcat.agriculturerepository.activity.login.LoginPresenterApp;
import com.tomcat.agriculturerepository.activity.login.LoginView;
import com.tomcat.agriculturerepository.activity.main.MainPresenter;
import com.tomcat.agriculturerepository.activity.main.MainPresenterApp;
import com.tomcat.agriculturerepository.activity.main.MainView;
import com.tomcat.agriculturerepository.activity.main.datanamaproduk.DataNamaProdukAdapter;
import com.tomcat.agriculturerepository.activity.splash.SplashPresenter;
import com.tomcat.agriculturerepository.activity.splash.SplashPresenterApp;
import com.tomcat.agriculturerepository.activity.splash.SplashView;
import com.tomcat.agriculturerepository.activity.updatepassword.ChangePasswordPresenter;
import com.tomcat.agriculturerepository.activity.updatepassword.ChangePasswordPresenterApp;
import com.tomcat.agriculturerepository.activity.updatepassword.ChangePasswordView;
import com.tomcat.agriculturerepository.activity.updateprofile.ChangeProfilePresenter;
import com.tomcat.agriculturerepository.activity.updateprofile.ChangeProfilePresenterApp;
import com.tomcat.agriculturerepository.activity.updateprofile.ChangeProfileView;
import com.tomcat.agriculturerepository.data.network.model.DataNamaProduk;
import com.tomcat.agriculturerepository.data.network.model.DataProdukByNama;
import com.tomcat.agriculturerepository.data.network.model.ProdukComment;
import com.tomcat.agriculturerepository.data.network.model.ProdukFoto;
import com.tomcat.agriculturerepository.di.ActivityContext;
import com.tomcat.agriculturerepository.di.PerActivity;
import com.tomcat.agriculturerepository.utils.rx.AppSchedulerProvider;
import com.tomcat.agriculturerepository.utils.rx.SchedulerProvider;

import java.util.ArrayList;

import dagger.Module;
import dagger.Provides;
import io.reactivex.disposables.CompositeDisposable;

/**
 * Created by janisharali on 27/01/17.
 */

@Module
public class ActivityModule {

    private AppCompatActivity mActivity;

    private ArrayList<DataProdukByNama>produkList;

    public ActivityModule(AppCompatActivity activity) {
        this.mActivity = activity;
    }

    @Provides
    @ActivityContext
    Context provideContext() {
        return mActivity;
    }

    @Provides
    AppCompatActivity provideActivity() {
        return mActivity;
    }

    @Provides
    CompositeDisposable provideCompositeDisposable() {
        return new CompositeDisposable();
    }

    @Provides
    SchedulerProvider provideSchedulerProvider() {
        return new AppSchedulerProvider();
    }


    @Provides
    @PerActivity
    SplashPresenter<SplashView> provideSplashPresenter(
            SplashPresenterApp<SplashView> presenter) {
        return presenter;
    }

    @Provides
    @PerActivity
    LoginPresenter<LoginView> provideLoginPresenter(
            LoginPresenterApp<LoginView> presenter) {
        return presenter;
    }

    @Provides
    @PerActivity
    MainPresenter<MainView> provideMainPresenter(
            MainPresenterApp<MainView> presenter) {
        return presenter;
    }

    @Provides
    AboutPresenter<AboutView> provideAboutPresenter(
            AboutPresenterApp<AboutView> presenter) {
        return presenter;
    }

//    @Provides
//    DataNamaProdukAdapter provideDataNamaProdukAdapter() {
//        return new DataNamaProdukAdapter(new MainActivity().getApplicationContext(), R.layout.item_data_nama_produk_view, produkList);
//    }

    @Provides
    DataNamaProdukAdapter provideDataNamaProdukAdapter() {
        return new DataNamaProdukAdapter(new ArrayList<DataNamaProduk>());
    }


    @Provides
    DataProdukByNamaPresenter<DataProdukByNamaView> provideDataProdukPresenter(
            DataProdukByNamaPresenterApp<DataProdukByNamaView> presenter) {
        return presenter;
    }

    @Provides
    DataProdukByNamaAdapter provideDataProdukAdapter() {
        return new DataProdukByNamaAdapter(new ArrayList<DataProdukByNama>());
    }

    @Provides
    DetailDataProdukPresenter<DetailDataProdukView> provideDetailDataProdukPresenter(
            DetailDataProdukPresenterApp<DetailDataProdukView> presenter) {
        return presenter;
    }

    @Provides
    AddDataProdukPresenter<AddDataProdukView> provideAddDataProdukPresenter(
            AddDataProdukPresenterApp<AddDataProdukView> presenter) {
        return presenter;
    }

    @Provides
    ChangePasswordPresenter<ChangePasswordView> provideChangePasswordPresenter(
            ChangePasswordPresenterApp<ChangePasswordView> presenter) {
        return presenter;
    }

    @Provides
    ChangeProfilePresenter<ChangeProfileView> provideChangeProfilePresenter(
            ChangeProfilePresenterApp<ChangeProfileView> presenter) {
        return presenter;
    }

    @Provides
    KomentarProdukAdapter provideProdukKomentarAdapter() {
        return new KomentarProdukAdapter(new ArrayList<ProdukComment>());
    }

    @Provides
    PhotoProdukAdapter provideProdukPhotoAdapter() {
        return new PhotoProdukAdapter(new ArrayList<ProdukFoto>());
    }

    //
//    @Provides
//    @PerActivity
//    LoginMvpPresenter<LoginMvpView> provideLoginPresenter(
//            LoginPresenter<LoginMvpView> presenter) {
//        return presenter;
//    }
//
//    @Provides
//    @PerActivity
//    MainPresenter<MainMvpView> provideMainPresenter(
//            MainPresenterApp<MainMvpView> presenter) {
//        return presenter;
//    }
//
//    @Provides
//    RatingDialogMvpPresenter<RatingDialogMvpView> provideRateUsPresenter(
//            RatingDialogPresenter<RatingDialogMvpView> presenter) {
//        return presenter;
//    }
//
//    @Provides
//    FeedMvpPresenter<FeedMvpView> provideFeedPresenter(
//            FeedPresenter<FeedMvpView> presenter) {
//        return presenter;
//    }
//
//    @Provides
//    OpenSourceMvpPresenter<OpenSourceMvpView> provideOpenSourcePresenter(
//            OpenSourcePresenter<OpenSourceMvpView> presenter) {
//        return presenter;
//    }
//
//    @Provides
//    DataProdukByNamaPresenter<DataProdukByNamaView> provideBlogMvpPresenter(
//            DataProdukByNamaPresenterApp<DataProdukByNamaView> presenter) {
//        return presenter;
//    }
//
//    @Provides
//    FeedPagerAdapter provideFeedPagerAdapter(AppCompatActivity activity) {
//        return new FeedPagerAdapter(activity.getSupportFragmentManager());
//    }
//
//    @Provides
//    OpenSourceAdapter provideOpenSourceAdapter() {
//        return new OpenSourceAdapter(new ArrayList<OpenSourceResponse.Repo>());
//    }
//
//    @Provides
//    DataNamaProdukAdapter provideBlogAdapter() {
//        return new DataNamaProdukAdapter(new ArrayList<DataNamaProduk>());
//    }
//
    @Provides
    LinearLayoutManager provideLinearLayoutManager(AppCompatActivity activity) {
        return new LinearLayoutManager(activity);
    }
}
