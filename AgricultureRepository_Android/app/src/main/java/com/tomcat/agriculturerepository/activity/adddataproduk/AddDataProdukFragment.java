/*
 * Copyright (C) 2017 MINDORKS NEXTGEN PRIVATE LIMITED
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://mindorks.com/license/apache-v2
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */

package com.tomcat.agriculturerepository.activity.adddataproduk;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Animatable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.NavUtils;
import android.support.v4.app.TaskStackBuilder;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.tomcat.agriculturerepository.R;
import com.tomcat.agriculturerepository.activity.base.BaseActivity;
import com.tomcat.agriculturerepository.activity.base.BaseFragment;
import com.tomcat.agriculturerepository.activity.dataprodukbynama.DataProdukByNamaByNamaFragment;
import com.tomcat.agriculturerepository.activity.main.MainActivity;
import com.tomcat.agriculturerepository.di.component.ActivityComponent;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by janisharali on 25/05/17.
 */

public class AddDataProdukFragment extends BaseFragment implements
        AddDataProdukView, View.OnClickListener  {

    public static final String TAG = "DetailDataProdukFragment";

    public static final String TAG_PASS_ID_DATA_PRODUK = "id_data_produk";

    @BindView(R.id.toolbar)
    Toolbar mToolbar;

    @Inject
    AddDataProdukPresenter<AddDataProdukView> mPresenter;

    @BindView(R.id.status_berhasil_text_view)
    TextView statusBerhasilEditText;

    @BindView(R.id.radio_status_berhasil)
    RadioGroup statusBerhasilRadioGroup;

    @BindView(R.id.radio_berhasil)
    RadioButton berhasilRadioButton;

    @BindView(R.id.radio_gagal)
    RadioButton gagalRadioButton;

    @BindView(R.id.id_nama_produk_edit_text)
    EditText idNamaProdukEditText;

    @BindView(R.id.tanggal_tanam_edit_text)
    EditText tanggalTanamEditText;

    @BindView(R.id.tanggal_panen_edit_text)
    EditText tanggalPanenEditText;

    @BindView(R.id.luas_lahan_edit_text)
    EditText luasLahanEditText;

    @BindView(R.id.lokasi_edit_text)
    EditText lokasiEditText;

//    @BindView(R.id.cuaca_edit_text)
//    EditText cuacaEditText;

    @BindView(R.id.suhu_edit_text)
    EditText suhuEditText;

    @BindView(R.id.harga_bibit_edit_text)
    EditText hargaEditText;

    @BindView(R.id.id_satuan_harga_bibit_edit_text)
    EditText idSatuanHargaBibitEditText;

    @BindView(R.id.harga_jual_edit_text)
    EditText hargaJualEditText;

    @BindView(R.id.id_satuan_harga_jual_edit_text)
    EditText idSatuanHargaJualEditText;

    @BindView(R.id.link_foto_edit_text)
    EditText linkFotoEditText;

    @BindView(R.id.id_penyakit_edit_text)
    EditText idPenyakitEditText;

    @BindView(R.id.id_pupuk_edit_text)
    EditText idPupukEditText;

    public static AddDataProdukFragment newInstance() {
        Bundle args = new Bundle();
        AddDataProdukFragment fragment = new AddDataProdukFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_add_data_produk, container, false);

        ActivityComponent component = getActivityComponent();
        if (component != null) {
            component.inject(this);
            setUnBinder(ButterKnife.bind(this, view));
            mPresenter.onAttach(this);
        }

        ((AppCompatActivity)getActivity()).setSupportActionBar(mToolbar);
        ((MainActivity)getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setHasOptionsMenu(true);

        return view;
    }

    @Override
    protected void setUp(View view) {
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        setDummy();
    }
    void setDummy() {
        idNamaProdukEditText.setText("1");
        tanggalTanamEditText.setText("2017-01-01");
        tanggalPanenEditText.setText("2017-05-05");
        luasLahanEditText.setText("500");
        lokasiEditText.setText("Jalan Riau 1");
        suhuEditText.setText("30");
        hargaEditText.setText("5000");
        idSatuanHargaBibitEditText.setText("1");
        hargaJualEditText.setText("10000");
        idSatuanHargaJualEditText.setText("1");
        linkFotoEditText.setText("https://manfaat.co.id/wp-content/uploads/2014/08/kentang.jpg");
        idPenyakitEditText.setText("2");
        idPupukEditText.setText("1");
    }

    @OnClick(R.id.btn_send_add_produk)
    @Override public void onClick(View v) {
        int idRadio = statusBerhasilRadioGroup.getCheckedRadioButtonId();
        String selected = ((RadioButton)getActivity().findViewById(idRadio)).getText().toString();

        boolean status;
        if(selected.equals("Berhasil")) {
            status = true;
            Log.d("AAAAAA","y");
        }
        else {
            status = false;
            Log.d("AAAAAA","n");
        }
        mPresenter.sendAddProduk(
                status
                , idNamaProdukEditText.getText().toString()
                , tanggalTanamEditText.getText().toString()
                , tanggalPanenEditText.getText().toString()
                , luasLahanEditText.getText().toString()
                , lokasiEditText.getText().toString()
                , suhuEditText.getText().toString()
                , hargaEditText.getText().toString()
                , idSatuanHargaBibitEditText.getText().toString()
                , hargaJualEditText.getText().toString()
                , idSatuanHargaJualEditText.getText().toString()
                , linkFotoEditText.getText().toString()
                , idPenyakitEditText.getText().toString()
                , idPupukEditText.getText().toString()
        );
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Drawable drawable = item.getIcon();
        if (drawable instanceof Animatable) {
            ((Animatable) drawable).start();
        }
        switch (item.getItemId()) {
            case android.R.id.home:
                getBaseActivity().onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void navigateDoneAddActivity() {
        ((MainActivity)getActivity()).updateContent();
        getBaseActivity().onFragmentDetached(TAG);
    }

    @Override
    public void onDestroyView() {
        mPresenter.onDetach();
        super.onDestroyView();
    }
}
