package com.tomcat.agriculturerepository.activity.login;

import android.content.Context;
import android.os.Handler;

import com.androidnetworking.error.ANError;
import com.tomcat.agriculturerepository.R;
import com.tomcat.agriculturerepository.activity.base.BasePresenterApp;
import com.tomcat.agriculturerepository.activity.splash.SplashPresenter;
import com.tomcat.agriculturerepository.activity.splash.SplashView;
import com.tomcat.agriculturerepository.data.DataManager;
import com.tomcat.agriculturerepository.data.network.model.LoginRequest;
import com.tomcat.agriculturerepository.data.network.model.LoginResponse;
import com.tomcat.agriculturerepository.utils.CommonUtils;
import com.tomcat.agriculturerepository.utils.rx.SchedulerProvider;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;

/**
 * Created by albertbrucelee on 21/12/17.
 */

public class LoginPresenterApp <V extends LoginView> extends BasePresenterApp<V>
        implements LoginPresenter<V> {

    @Inject
    public LoginPresenterApp(DataManager dataManager,
                             SchedulerProvider schedulerProvider,
                             CompositeDisposable compositeDisposable) {
        super(dataManager, schedulerProvider, compositeDisposable);
    }

//    public LoginPresenterApp(Context context){
//        super(context);
//    }

    @Override public void validateCredentials(String email, String password) {
        //validate email and password
        if (email == null || email.isEmpty()) {
            getView().onError(R.string.empty_email);
            return;
        }
        if (!CommonUtils.isEmailValid(email)) {
            getView().onError(R.string.invalid_email);
            return;
        }
        if (password == null || password.isEmpty()) {
            getView().onError(R.string.empty_password);
            return;
        }
        getView().showLoading();

        getCompositeDisposable().add(getDataManager()
                .doServerLoginApiCall(new LoginRequest.ServerLoginRequest(email, password))
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(new Consumer<LoginResponse>() {
                    @Override
                    public void accept(LoginResponse response) throws Exception {
                        getDataManager().updateUserInfo(
                                response.getAccessToken(),
                                response.getUserId(),
                                DataManager.LoggedInMode.LOGGED_IN_MODE_SERVER,
                                response.getUserName(),
                                response.getUserEmail(),
                                response.getGoogleProfilePicUrl());

                        if (!isViewAttached()) {
                            return;
                        }

                        getView().hideLoading();
                        getView().openMainActivity();

                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {

                        if (!isViewAttached()) {
                            return;
                        }

                        getView().hideLoading();

                        // handle the login error here
                        if (throwable instanceof ANError) {
                            ANError anError = (ANError) throwable;
                            handleApiError(anError);
                        }
                    }
                }));

    }

//    @Override public void onUsernameError() {
//        if (isViewAttached()) {
//            getView().setUsernameError();
//            getView().hideProgress();
//        }
//    }
//
//    @Override public void onPasswordError() {
//        if (isViewAttached()) {
//            getView().setPasswordError();
//            getView().hideProgress();
//        }
//    }

//    @Override public void onSuccess() {
//        if (isViewAttached()) {
//            getView().navigateToHome();
//        }
//    }

}