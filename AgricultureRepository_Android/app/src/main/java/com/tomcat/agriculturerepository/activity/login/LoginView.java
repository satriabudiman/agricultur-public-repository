package com.tomcat.agriculturerepository.activity.login;

import com.tomcat.agriculturerepository.activity.base.BaseView;

/**
 * Created by albertbrucelee on 21/12/17.
 */

public interface LoginView extends BaseView {

//    void showProgress();
//
//    void hideProgress();

//    void setUsernameError();
//
//    void setPasswordError();

    void openMainActivity();

}

