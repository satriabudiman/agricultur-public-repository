/*
 * Copyright (C) 2017 MINDORKS NEXTGEN PRIVATE LIMITED
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://mindorks.com/license/apache-v2
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */

package com.tomcat.agriculturerepository.data.network;


import com.tomcat.agriculturerepository.data.network.model.AddDataProdukResponse;
import com.tomcat.agriculturerepository.data.network.model.DataNamaProdukResponse;
import com.tomcat.agriculturerepository.data.network.model.DataProduk;
import com.tomcat.agriculturerepository.data.network.model.DataProdukByNamaRequest;
import com.tomcat.agriculturerepository.data.network.model.DataProdukByNamaResponse;
import com.tomcat.agriculturerepository.data.network.model.DeleteDataProdukRequest;
import com.tomcat.agriculturerepository.data.network.model.DeleteDataProdukResponse;
import com.tomcat.agriculturerepository.data.network.model.DetailDataProduk;
import com.tomcat.agriculturerepository.data.network.model.DetailDataProdukRequest;
import com.tomcat.agriculturerepository.data.network.model.DetailDataProdukResponse;
import com.tomcat.agriculturerepository.data.network.model.LoginRequest;
import com.tomcat.agriculturerepository.data.network.model.LoginResponse;
import com.tomcat.agriculturerepository.data.network.model.LogoutResponse;
import com.tomcat.agriculturerepository.data.network.model.UpdatePasswordRequest;
import com.tomcat.agriculturerepository.data.network.model.UpdatePasswordResponse;
import com.tomcat.agriculturerepository.data.network.model.UpdateProfileRequest;
import com.tomcat.agriculturerepository.data.network.model.UpdateProfileResponse;

import io.reactivex.Observable;

/**
 * Created by janisharali on 27/01/17.
 */

public interface ApiHelper {

    ApiHeader getApiHeader();

    Observable<LoginResponse> doGoogleLoginApiCall(LoginRequest.GoogleLoginRequest request);

    Observable<LoginResponse> doFacebookLoginApiCall(LoginRequest.FacebookLoginRequest request);

    Observable<LoginResponse> doServerLoginApiCall(LoginRequest.ServerLoginRequest request);

    Observable<UpdatePasswordResponse> doServerUpdatePasswordApiCall(UpdatePasswordRequest.ServerUpdatePasswordRequest request);

    Observable<UpdateProfileResponse> doServerUpdateProfileApiCall(UpdateProfileRequest.ServerUpdateProfileRequest request);

    Observable<LogoutResponse> doLogoutApiCall();

    Observable<DataNamaProdukResponse> getDataNamaProdukApiCall();

    Observable<DataProdukByNamaResponse> getDataProdukApiCall(DataProdukByNamaRequest request);

    Observable<DetailDataProdukResponse> getDetailDataProdukApiCall(DetailDataProdukRequest request);

    Observable<AddDataProdukResponse> addDataProdukApiCall(DataProduk
                                                                   request);

    Observable<DeleteDataProdukResponse> deleteDataProdukApiCall(DeleteDataProdukRequest
                                                                         request);
}
