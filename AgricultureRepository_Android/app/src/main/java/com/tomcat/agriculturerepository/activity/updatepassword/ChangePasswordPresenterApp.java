package com.tomcat.agriculturerepository.activity.updatepassword;

import com.androidnetworking.error.ANError;
import com.tomcat.agriculturerepository.R;
import com.tomcat.agriculturerepository.activity.base.BasePresenterApp;
import com.tomcat.agriculturerepository.data.DataManager;
import com.tomcat.agriculturerepository.data.network.model.DataNamaProdukResponse;
import com.tomcat.agriculturerepository.data.network.model.LoginRequest;
import com.tomcat.agriculturerepository.data.network.model.LoginResponse;
import com.tomcat.agriculturerepository.data.network.model.LogoutResponse;
import com.tomcat.agriculturerepository.data.network.model.UpdatePasswordRequest;
import com.tomcat.agriculturerepository.data.network.model.UpdatePasswordResponse;
import com.tomcat.agriculturerepository.utils.CommonUtils;
import com.tomcat.agriculturerepository.utils.rx.SchedulerProvider;

import javax.inject.Inject;

import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;

/**
 * Created by Michael on 26/02/2017.
 */

public class ChangePasswordPresenterApp<V extends ChangePasswordView> extends BasePresenterApp<V>
        implements ChangePasswordPresenter<V> {

    @Inject
    public ChangePasswordPresenterApp(DataManager dataManager,
                             SchedulerProvider schedulerProvider,
                             CompositeDisposable compositeDisposable) {
        super(dataManager, schedulerProvider, compositeDisposable);
    }

//    public LoginPresenterApp(Context context){
//        super(context);
//    }

    @Override public void updatePassword(Long id, String oldPassword, String newPassword) {
        //validate email and password
        if (id == null) {
            getView().onError("Please provide a non empty id");
            return;
        }
        if (oldPassword == null || oldPassword.isEmpty()) {
            getView().onError("Please provide a non empty current password");
            return;
        }
        if (newPassword == null || newPassword.isEmpty()) {
            getView().onError("Please provide a non empty new password");
            return;
        }
        getView().showLoading();

        getCompositeDisposable().add(getDataManager()
                .doServerUpdatePasswordApiCall(new UpdatePasswordRequest.ServerUpdatePasswordRequest(id, oldPassword, newPassword))
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(new Consumer<UpdatePasswordResponse>() {
                    @Override
                    public void accept(UpdatePasswordResponse response) throws Exception {
                        getDataManager().updatePasswordUser(
                                response.getMessage());

                        if (!isViewAttached()) {
                            return;
                        }

                        getView().hideLoading();
                        getView().updatePasswordSuccess();

                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {

                        if (!isViewAttached()) {
                            return;
                        }

                        getView().hideLoading();

                        // handle the login error here
                        if (throwable instanceof ANError) {
                            ANError anError = (ANError) throwable;
                            handleApiError(anError);
                        }
                    }
                }));

    }
}
