package com.tomcat.agriculturerepository.activity.updateprofile;

import com.tomcat.agriculturerepository.activity.base.BaseView;

/**
 * Created by Michael on 26/02/2017.
 */

public interface ChangeProfileView extends BaseView {
    void updateProfileSuccess();
}
