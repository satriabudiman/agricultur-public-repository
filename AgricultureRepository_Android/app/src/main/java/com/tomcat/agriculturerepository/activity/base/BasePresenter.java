package com.tomcat.agriculturerepository.activity.base;

import com.androidnetworking.error.ANError;

/**
 * Created by albertbrucelee on 21/12/17.
 */

public interface BasePresenter <V extends BaseView> {

    void onAttach(V mvpView);

    void onDetach();

    void handleApiError(ANError error);

    void setUserAsLoggedOut();

}
