/*
 * Copyright (C) 2017 MINDORKS NEXTGEN PRIVATE LIMITED
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://mindorks.com/license/apache-v2
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */

package com.tomcat.agriculturerepository.activity.dataprodukbynama;

import android.graphics.drawable.Animatable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.tomcat.agriculturerepository.R;
import com.tomcat.agriculturerepository.activity.base.BaseFragment;
import com.tomcat.agriculturerepository.activity.detaildataproduk.DetailDataProdukFragment;
import com.tomcat.agriculturerepository.activity.main.MainActivity;
import com.tomcat.agriculturerepository.data.network.model.DataProdukByNama;
import com.tomcat.agriculturerepository.di.component.ActivityComponent;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by janisharali on 25/05/17.
 */

public class DataProdukByNamaByNamaFragment extends BaseFragment implements
        DataProdukByNamaView, DataProdukByNamaAdapter.Callback {

    public static final String TAG = "DataProdukByNamaByNamaFragment";

    public static final String TAG_PASS_ID_NAMA_PRODUK = "id_nama_produk";

    @BindView(R.id.toolbar)
    Toolbar mToolbar;

    @Inject
    DataProdukByNamaPresenter<DataProdukByNamaView> mPresenter;

    @Inject
    DataProdukByNamaAdapter mDataProdukAdapter;

    @Inject
    LinearLayoutManager mLayoutManager;

    @BindView(R.id.data_produk_recycler_view)
    RecyclerView mRecyclerView;

    public static DataProdukByNamaByNamaFragment newInstance(String idNamaProduk) {
        Bundle args = new Bundle();
        args.putString(TAG_PASS_ID_NAMA_PRODUK, idNamaProduk);
        DataProdukByNamaByNamaFragment fragment = new DataProdukByNamaByNamaFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_data_produk, container, false);

        ActivityComponent component = getActivityComponent();
        if (component != null) {
            component.inject(this);
            setUnBinder(ButterKnife.bind(this, view));
            mPresenter.onAttach(this);
            mPresenter.setIdNamaProduk(this.getArguments().getString(TAG_PASS_ID_NAMA_PRODUK));
            mDataProdukAdapter.setCallback(this);
        }

        ((AppCompatActivity)getActivity()).setSupportActionBar(mToolbar);
        ((MainActivity)getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setHasOptionsMenu(true);

        return view;
    }

    @Override
    protected void setUp(View view) {
        //set this to avoid clicked parent if we click anything on this fragment view
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            }
        });

        mLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mRecyclerView.setAdapter(mDataProdukAdapter);

        mPresenter.onViewPrepared();
    }

    @Override
    public void onEmptyViewRetryClick() {
        mPresenter.onViewPrepared();
    }

    @Override
    public void updateDataProduk(List<DataProdukByNama> dataProdukByNamaList) {
        mDataProdukAdapter.addItems(dataProdukByNamaList);
    }

    @Override
    public void onItemClick(DataProdukByNama dataProdukByNama) {
        showDetailDataProdukFragment(dataProdukByNama.getId());
    }

    @Override
    public void showDetailDataProdukFragment(int idProduk) {
        getActivity().getSupportFragmentManager()
                .beginTransaction()
                .disallowAddToBackStack()
                .setCustomAnimations(R.anim.slide_left, R.anim.slide_right)
                .add(R.id.cl_root_view, DetailDataProdukFragment.newInstance(idProduk), DetailDataProdukFragment.TAG)
                .commit();
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Drawable drawable = item.getIcon();
        if (drawable instanceof Animatable) {
            ((Animatable) drawable).start();
        }
        switch (item.getItemId()) {
            case android.R.id.home:
                getBaseActivity().onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onDestroyView() {
        mPresenter.onDetach();
        super.onDestroyView();
    }
}
