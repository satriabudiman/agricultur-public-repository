package com.tomcat.agriculturerepository.data.network.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by albertbrucelee on 26/12/17.
 */

public class SatuanHargaJual {

    @Expose
    @SerializedName("id")
    private Integer id;

    @Expose
    @SerializedName("name")
    private String nama;

    public Integer getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }


    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof SatuanHargaJual)) return false;

        SatuanHargaJual produkPenyakit = (SatuanHargaJual) o;

        if (!id.equals(produkPenyakit.id)) return false;
        return nama.equals(produkPenyakit.nama);
    }

    @Override
    public int hashCode() {
        int result = id.hashCode();
        result = 31 * result + nama.hashCode();
        return result;
    }
}