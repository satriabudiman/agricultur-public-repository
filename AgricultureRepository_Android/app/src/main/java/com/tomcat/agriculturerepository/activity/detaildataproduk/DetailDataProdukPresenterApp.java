/*
 * Copyright (C) 2017 MINDORKS NEXTGEN PRIVATE LIMITED
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://mindorks.com/license/apache-v2
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */

package com.tomcat.agriculturerepository.activity.detaildataproduk;

import android.util.Log;

import com.androidnetworking.error.ANError;
import com.tomcat.agriculturerepository.R;
import com.tomcat.agriculturerepository.activity.base.BasePresenterApp;
import com.tomcat.agriculturerepository.data.DataManager;
import com.tomcat.agriculturerepository.data.network.model.AddDataProdukResponse;
import com.tomcat.agriculturerepository.data.network.model.DataProduk;
import com.tomcat.agriculturerepository.data.network.model.DeleteDataProdukRequest;
import com.tomcat.agriculturerepository.data.network.model.DeleteDataProdukResponse;
import com.tomcat.agriculturerepository.data.network.model.DetailDataProduk;
import com.tomcat.agriculturerepository.data.network.model.DetailDataProdukRequest;
import com.tomcat.agriculturerepository.data.network.model.DetailDataProdukResponse;
import com.tomcat.agriculturerepository.data.network.model.ProdukFoto;
import com.tomcat.agriculturerepository.utils.AppConstants;
import com.tomcat.agriculturerepository.utils.rx.SchedulerProvider;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;

/**
 * Created by janisharali on 25/05/17.
 */

public class DetailDataProdukPresenterApp<V extends DetailDataProdukView> extends BasePresenterApp<V>
        implements DetailDataProdukPresenter<V> {

    public int idDataProduk;

    @Inject
    public DetailDataProdukPresenterApp(DataManager dataManager,
                                        SchedulerProvider schedulerProvider,
                                        CompositeDisposable compositeDisposable) {
        super(dataManager, schedulerProvider, compositeDisposable);
    }

    @Override
    public void setIdDataProduk(int idDataProduk){
        this.idDataProduk = idDataProduk;
    }

    @Override
    public void onViewPrepared() {
        getView().showLoading();
        getCompositeDisposable().add(getDataManager()
                .getDetailDataProdukApiCall(new DetailDataProdukRequest(idDataProduk))
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(new Consumer<DetailDataProdukResponse>() {
                    @Override
                    public void accept(@NonNull DetailDataProdukResponse detailDataProdukResponse)
                            throws Exception {
                        if (detailDataProdukResponse != null) {
                            DetailDataProduk detailDataProduk = detailDataProdukResponse.getData();
                            if(detailDataProduk != null) {
                                getView().updateDetailDataProduk(detailDataProduk);
                            }
                            if(detailDataProduk.getProdukFotoList() != null) {
                                getView().updateProdukFoto(detailDataProduk.getProdukFotoList());
                            }
                            if(detailDataProduk.getProdukPenyakitList() != null) {
                                getView().updateProdukPenyakit(detailDataProduk.getProdukPenyakitList());
                            }
                            if(detailDataProduk.getProdukPupukList() != null) {
                                getView().updateProdukPupuk(detailDataProduk.getProdukPupukList());
                            }
                            if(detailDataProduk.getProdukCommentList() != null) {
                                getView().updateProdukComment(detailDataProduk.getProdukCommentList());
                            }
                        }
                        getView().hideLoading();
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(@NonNull Throwable throwable)
                            throws Exception {
                        if (!isViewAttached()) {
                            return;
                        }

                        getView().hideLoading();

                        // handle the error here
                        if (throwable instanceof ANError) {
                            ANError anError = (ANError) throwable;
                            handleApiError(anError);
                        }
                    }
                }));
    }

    public void deleteProduk() {
        getView().showLoading();

        getCompositeDisposable().add(getDataManager()
                .deleteDataProdukApiCall(new DeleteDataProdukRequest(idDataProduk))
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(new Consumer<DeleteDataProdukResponse>() {
                    @Override
                    public void accept(DeleteDataProdukResponse response) throws Exception {
                        if (!isViewAttached()) {
                            return;
                        }

                        getView().hideLoading();
                        if(response.getStatusCode().equals(AppConstants.STATUS_CODE_SUCCESS)) {
                            getView().showMessage(R.string.message_success_delete_data_produk);
                            getView().navigateDoneDeleteActivity();
                        } else {
                            getView().showMessage(response.getMessage());
                        }

                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {

                        if (!isViewAttached()) {
                            return;
                        }

                        getView().hideLoading();

                        // handle the login error here
                        if (throwable instanceof ANError) {
                            ANError anError = (ANError) throwable;
                            handleApiError(anError);
                        }
                    }
                }));
    }

}
