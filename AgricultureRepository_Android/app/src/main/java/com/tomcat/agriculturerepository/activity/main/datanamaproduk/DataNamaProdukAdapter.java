/*
 * Copyright (C) 2017 MINDORKS NEXTGEN PRIVATE LIMITED
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://mindorks.com/license/apache-v2
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */

package com.tomcat.agriculturerepository.activity.main.datanamaproduk;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.tomcat.agriculturerepository.R;
import com.tomcat.agriculturerepository.activity.base.BaseViewHolder;
import com.tomcat.agriculturerepository.data.network.model.DataNamaProduk;
import com.tomcat.agriculturerepository.data.network.model.DataNamaProdukResponse;
import com.tomcat.agriculturerepository.data.network.model.DataProdukByNamaResponse;
import com.tomcat.agriculturerepository.utils.AppLogger;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Janisharali on 25-05-2017.
 */

public class DataNamaProdukAdapter extends RecyclerView.Adapter<BaseViewHolder> {

    public static final int VIEW_TYPE_EMPTY = 0;
    public static final int VIEW_TYPE_NORMAL = 1;

    private Callback mCallback;
    private List<DataNamaProduk> mDataNamaProdukResponseList;

    public DataNamaProdukAdapter(List<DataNamaProduk> dataNamaProdukList) {
        mDataNamaProdukResponseList = dataNamaProdukList;
    }


    public void setCallback(Callback callback) {
        mCallback = callback;
    }

    @Override
    public void onBindViewHolder(BaseViewHolder holder, int position) {
        holder.onBind(position);
    }

    @Override
    public BaseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        switch (viewType) {
            case VIEW_TYPE_NORMAL:
                return new ViewHolder(
                        LayoutInflater.from(parent.getContext()).inflate(R.layout.item_data_nama_produk_view, parent, false));
            case VIEW_TYPE_EMPTY:
            default:
                return new EmptyViewHolder(
                        LayoutInflater.from(parent.getContext()).inflate(R.layout.item_empty_view, parent, false));
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (mDataNamaProdukResponseList != null && mDataNamaProdukResponseList.size() > 0) {
            return VIEW_TYPE_NORMAL;
        } else {
            return VIEW_TYPE_EMPTY;
        }
    }

    @Override
    public int getItemCount() {
        if (mDataNamaProdukResponseList != null && mDataNamaProdukResponseList.size() > 0) {
            return mDataNamaProdukResponseList.size();
        } else {
            return 1;
        }
    }

    public void addItems(List<DataNamaProduk> dataNamaProdukList) {
        mDataNamaProdukResponseList.addAll(dataNamaProdukList);
        notifyDataSetChanged();
    }

    public void clearItems() {
        mDataNamaProdukResponseList.clear();
        notifyDataSetChanged();
    }

    public interface Callback {
        void updateContent();
        void onEmptyViewRetryClick();
        void onItemClick(DataNamaProduk dataNamaProduk);
    }

    public class ViewHolder extends BaseViewHolder {

        @BindView(R.id.produk_foto)
        ImageView fotoProduk;

        @BindView(R.id.nama_produk_text_view)
        TextView namaProdukTextView;

        @BindView(R.id.jumlah_data_text_view)
        TextView jumlahDataTextView;

        @BindView(R.id.keberhasilan_text_view)
        TextView persenBerhasilTextView;


        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        protected void clear() {
            namaProdukTextView.setText("");
        }

        public void onBind(int position) {
            super.onBind(position);

            final DataNamaProduk dataNamaProduk = mDataNamaProdukResponseList.get(position);

            if (dataNamaProduk.getNamaProduk() != null) {
                namaProdukTextView.setText(dataNamaProduk.getNamaProduk());
            }

            if (dataNamaProduk.getJumlahData() != null) {
                jumlahDataTextView.setText(dataNamaProduk.getJumlahData().concat(" data"));
            }

            if (dataNamaProduk.getPersenBerhasil() != null) {
                String persenBerhasil = String.valueOf(Float.valueOf(dataNamaProduk.getPersenBerhasil())*100);
                persenBerhasilTextView.setText(persenBerhasil.concat("% berhasil"));
            }

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (dataNamaProduk.getId() != null) {
                        mCallback.onItemClick(dataNamaProduk);
                    }
                }
            });
        }
    }

    public class EmptyViewHolder extends BaseViewHolder {

        @BindView(R.id.btn_retry)
        Button retryButton;

        @BindView(R.id.tv_message)
        TextView messageTextView;

        public EmptyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @Override
        protected void clear() {

        }

        @OnClick(R.id.btn_retry)
        void onRetryClick() {
            if (mCallback != null)
                mCallback.onEmptyViewRetryClick();
        }
    }
}
