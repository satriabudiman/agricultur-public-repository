package com.tomcat.agriculturerepository.activity.login;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.os.Bundle;
import android.support.v7.widget.AppCompatTextView;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;

import com.tomcat.agriculturerepository.R;
//import com.tomcat.agriculturerepository.activity.accountreset.ForgetPasswordActivity;
import com.tomcat.agriculturerepository.activity.base.BaseActivity;
import com.tomcat.agriculturerepository.activity.main.MainActivity;
//import com.tomcat.agriculturerepository.activity.register.RegisterActivity;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by albertbrucelee on 21/12/17.
 */

public class LoginActivity extends BaseActivity implements LoginView, View.OnClickListener {

    @Inject
    LoginPresenter<LoginView> presenter;

    @BindView(R.id.editTextUsername)
    EditText mEditTextEmail;

    @BindView(R.id.editTextPassword)
    EditText mEditTextPassword;

    @BindView(R.id.progress)
    ProgressBar progressBar;

    private AppCompatTextView TextSignup;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

//        ActionBar mactionBar = getSupportActionBar();
//        mactionBar.setSubtitle("Agriculture Repository");
//        mactionBar.setDisplayShowHomeEnabled(true);
//        mactionBar.setLogo(R.drawable.ic_logo);
//        mactionBar.setDisplayUseLogoEnabled(true);


//        TextSignup = (AppCompatTextView) findViewById(R.id.linkSignup);
//        TextSignup.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                startActivity(new Intent(getApplicationContext(), RegisterActivity.class));
//            }
//        });


        getActivityComponent().inject(this);

        setUnBinder(ButterKnife.bind(this));

        presenter.onAttach(this);
        mEditTextEmail.setText("albert@gmail.com");
        mEditTextPassword.setText("secret");
    }

    @Override protected void onDestroy() {
        presenter.onDetach();
        super.onDestroy();
    }


    public static Intent getStartIntent(Context context) {
        Intent intent = new Intent(context, LoginActivity.class);
        return intent;
    }


//    @Override public void showProgress() {
//        progressBar.setVisibility(View.VISIBLE);
//    }
//
//    @Override public void hideProgress() {
//        progressBar.setVisibility(View.GONE);
//    }
//
//    @Override public void setUsernameError() {
//        mEditTextEmail.setError(getString(R.string.));
//    }
//
//    @Override public void setPasswordError() {
//        mEditTextPassword.setError(getString(R.string.password_error));
//    }

    @Override public void openMainActivity() {
        Intent intent = MainActivity.getStartIntent(LoginActivity.this);
        startActivity(intent);
        finish();
    }

    @OnClick(R.id.buttonLogin)
    @Override public void onClick(View v) {
        presenter.validateCredentials(mEditTextEmail.getText().toString(), mEditTextPassword.getText().toString());
    }

//    @OnClick(R.id.linkForgetPassword)
//    void onForgetPassword(View v) {
//        startActivity(new Intent(getApplicationContext(), ForgetPasswordActivity.class));
//    }

    @Override
    protected void setUp() {

    }
}