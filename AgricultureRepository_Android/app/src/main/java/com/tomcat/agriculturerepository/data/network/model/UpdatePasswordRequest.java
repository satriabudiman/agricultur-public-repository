package com.tomcat.agriculturerepository.data.network.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Michael on 26/12/2017.
 */

public class UpdatePasswordRequest {
    private UpdatePasswordRequest() {
        // This class is not publicly instantiable
    }

    public static class ServerUpdatePasswordRequest {
        @Expose
        @SerializedName("id")
        private Long id;

        @Expose
        @SerializedName("old_password")
        private String oldPassword;

        @Expose
        @SerializedName("new_password")
        private String newPassword;

        public ServerUpdatePasswordRequest(Long id, String oldPassword, String newPassword) {
            this.id = id;
            this.oldPassword = oldPassword;
            this.newPassword = newPassword;
        }

        public Long getId() {
            return id;
        }

        public void setOldPassword(String email) {
            this.oldPassword = oldPassword;
        }

        public String getOldPassword() {
            return oldPassword;
        }

        public void setNewPassword(String password) {
            this.newPassword = newPassword;
        }

        public String getNewPassword() {
            return newPassword;
        }

        @Override
        public boolean equals(Object object) {
            if (this == object) return true;
            if (object == null || getClass() != object.getClass()) return false;

            UpdatePasswordRequest.ServerUpdatePasswordRequest that = (UpdatePasswordRequest.ServerUpdatePasswordRequest) object;

            if (id != null ? !id.equals(that.id) : that.id != null) return false;
            return (oldPassword != null ? oldPassword.equals(that.oldPassword) : that.oldPassword == null) && (newPassword != null ? newPassword.equals(that.newPassword) : that.newPassword == null);

        }

        @Override
        public int hashCode() {
            int result = id != null ? id.hashCode() : 0;
            result = 31 * result + (oldPassword != null ? oldPassword.hashCode() : 0);
            result = 31 * result + (newPassword != null ? newPassword.hashCode() : 0);
            return result;
        }
    }
}
