/*
 * Copyright (C) 2017 MINDORKS NEXTGEN PRIVATE LIMITED
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://mindorks.com/license/apache-v2
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */

package com.tomcat.agriculturerepository.data.network.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by janisharali on 08/01/17.
 */

public class DataProdukByNamaRequest {

    @Expose
    @SerializedName("id")
    private String idNamaProduk;

    public DataProdukByNamaRequest(String idNamaProduk) {
        this.idNamaProduk = idNamaProduk;
    }

    public String getIdNamaProduk() {
        return idNamaProduk;
    }

    public void setIdNamaProduk(String idNamaProduk) {
        this.idNamaProduk = idNamaProduk;
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) return true;
        if (object == null || getClass() != object.getClass()) return false;

        DataProdukByNamaRequest that = (DataProdukByNamaRequest) object;

        return idNamaProduk != null ? idNamaProduk.equals(that.idNamaProduk) : that.idNamaProduk == null;

    }

    @Override
    public int hashCode() {
        int result = idNamaProduk != null ? idNamaProduk.hashCode() : 0;
        return result;
    }
}
