package com.tomcat.agriculturerepository.data.network.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by albertbrucelee on 26/12/17.
 */

public class ProdukFoto {

    @Expose
    @SerializedName("id")
    private String id;

    @Expose
    @SerializedName("url")
    private String url;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ProdukFoto)) return false;

        ProdukFoto produkFoto = (ProdukFoto) o;

        if (!id.equals(produkFoto.id)) return false;
        return url.equals(produkFoto.url);
    }

    @Override
    public int hashCode() {
        int result = id.hashCode();
        result = 31 * result + url.hashCode();
        return result;
    }
}