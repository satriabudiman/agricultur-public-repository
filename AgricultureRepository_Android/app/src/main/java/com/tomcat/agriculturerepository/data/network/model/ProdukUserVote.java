package com.tomcat.agriculturerepository.data.network.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by albertbrucelee on 26/12/17.
 */

public class ProdukUserVote {
    @Expose
    @SerializedName("id")
    private String idUser;

    public String getIdUser() {
        return idUser;
    }

    public void setIdUser(String idUser) {
        this.idUser = idUser;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ProdukUserVote)) return false;

        ProdukUserVote penyakitProduk = (ProdukUserVote) o;

        return idUser.equals(penyakitProduk.idUser);
    }

    @Override
    public int hashCode() {
        int result = idUser.hashCode();
        return result;
    }
}