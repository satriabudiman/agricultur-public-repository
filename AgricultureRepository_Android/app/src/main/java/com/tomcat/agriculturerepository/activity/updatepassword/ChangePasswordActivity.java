package com.tomcat.agriculturerepository.activity.updatepassword;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;

import com.tomcat.agriculturerepository.R;
import com.tomcat.agriculturerepository.activity.base.BaseActivity;
import com.tomcat.agriculturerepository.data.db.model.User;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Michael on 26/02/2017.
 */

public class ChangePasswordActivity extends BaseActivity implements ChangePasswordView, View.OnClickListener {
    @Inject
    ChangePasswordPresenter<ChangePasswordView> presenter;

    @BindView(R.id.editTextOldPassword)
    EditText mEditTextOldPassword;

    @BindView(R.id.editTextNewPassword)
    EditText mEditTextNewPassword;

    @BindView(R.id.editTextConfirmNewPassword)
    EditText mEditTextConfirmNewPassword;

    @BindView(R.id.buttonUpdate)
    Button mButtonUpdate;

    @BindView(R.id.progress)
    ProgressBar progressBar;

    User user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);

//        ActionBar mactionBar = getSupportActionBar();
//        mactionBar.setSubtitle("Agriculture Repository");
//        mactionBar.setDisplayShowHomeEnabled(true);
//        mactionBar.setLogo(R.drawable.ic_logo);
//        mactionBar.setDisplayUseLogoEnabled(true);


//        TextSignup = (AppCompatTextView) findViewById(R.id.linkSignup);
//        TextSignup.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                startActivity(new Intent(getApplicationContext(), RegisterActivity.class));
//            }
//        });


        getActivityComponent().inject(this);

        setUnBinder(ButterKnife.bind(this));

        presenter.onAttach(this);
    }

    @Override protected void onDestroy() {
        presenter.onDetach();
        super.onDestroy();
    }

    public static Intent getStartIntent(Context context) {
        Intent intent = new Intent(context, ChangePasswordActivity.class);
        return intent;
    }

    @Override public void updatePasswordSuccess() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
        alertDialog.setTitle("Success");
        alertDialog.setMessage("Password has been changed");
        alertDialog.setNegativeButton("Close", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                finish();
            }
        });
        alertDialog.show();
    }

    @OnClick(R.id.buttonUpdate)
    @Override public void onClick(View v) {
        if(mEditTextNewPassword.getText().toString().equals(mEditTextConfirmNewPassword.getText().toString())) {
            presenter.updatePassword(user.getId(), mEditTextOldPassword.getText().toString(), mEditTextNewPassword.getText().toString());
        }
        else {
            mEditTextConfirmNewPassword.setError("Confirmation password not match");
        }
    }

    @Override
    protected void setUp() {

    }


    /*private void updateData(){
        final String old_password = editTextOldPassword.getText().toString().trim();
        final String new_password = editTextNewPassword.getText().toString().trim();
        final String confirmNewPassword = editTextConfirmNewPassword.getText().toString().trim();

        if (new_password.equals(confirmNewPassword)) {

            class UpdateData extends AsyncTask<Void, Void, String> {
                ProgressDialog loading;

                @Override
                protected void onPreExecute() {
                    super.onPreExecute();
                    loading = ProgressDialog.show(ChangeProfileActivity.this, "Updating...", "Please wait...", true, false);
                }

                @Override
                protected void onPostExecute(String s) {
                    super.onPostExecute(s);
                    loading.dismiss();

                    s = s.replaceFirst(" ","");
                    s = s.trim();
                    if (s.equals("Sukses")) {
                        //Toast.makeText(getApplicationContext(), "Gagal, silahkan cek koneksi internet anda", Toast.LENGTH_LONG).show();
                        user.setPassword(new_password);
                        showAlertSuccess();
                    }
                    if (s.equals("Gagal")) {
                        //Toast.makeText(getApplicationContext(), "Gagal, silahkan cek koneksi internet anda", Toast.LENGTH_LONG).show();
                        showAlertNotValidPassword();
                    }
                }

                @Override
                protected String doInBackground(Void... params) {
                    HashMap<String, String> hashMap = new HashMap<>();
                    hashMap.put(Config.KEY_EMAIL, user.getEmail());
                    hashMap.put(Config.KEY_OLD_PASSWORD, old_password);
                    hashMap.put(Config.KEY_NEW_PASSWORD, new_password);

                    RequestHandler rh = new RequestHandler();

                    String s = rh.sendPostRequest(Config.UPDATE_PASSWORD_URL, hashMap);

                    return s;
                }
            }

            UpdateData ue = new UpdateData();
            ue.execute();
        }

        else{
            //Toast.makeText(ChangeProfileActivity.this, "Password baru dan konfirmasi password baru tidak sesuai", Toast.LENGTH_LONG).show();
            showAlertNotValid();
        }
    }

    public void showAlertSuccess() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(ChangeProfileActivity.this);
        alertDialog.setTitle("Success");
        alertDialog.setMessage("Password has been changed");
        alertDialog.setNegativeButton("Tutup",new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                finish();
            }
        });
        alertDialog.show();
    }

    public void showAlertNotValidPassword() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(ChangeProfileActivity.this);
        alertDialog.setTitle("Failed");
        alertDialog.setMessage("Incorrect current password");
        alertDialog.setNegativeButton("Tutup",new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        alertDialog.show();
    }

    public void showAlertNotValid() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(ChangeProfileActivity.this);
        alertDialog.setTitle("Tidak Sesuai");
        alertDialog.setMessage("Password baru dan konfirmasi password baru tidak sesuai");
        alertDialog.setNegativeButton("Tutup",new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        alertDialog.show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //Adding our menu to toolbar
        getMenuInflater().inflate(R.menu.menu_config, menu);
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        menu.findItem(R.id.action_refresh).setVisible(false);
        menu.findItem(R.id.action_about).setVisible(false);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onClick(View v) {
        if(v == buttonUpdate){
            oldPassword = editTextOldPassword.getText().toString();
            if(oldPassword.length() == 0) {
                editTextOldPassword.setError("Current password is required!");
            }

            newPassword = editTextNewPassword.getText().toString();
            if(newPassword.length() == 0) {
                editTextNewPassword.setError("New password is required!");
            }

            confirmNewPassword = editTextConfirmNewPassword.getText().toString();
            if(confirmNewPassword.length() == 0) {
                editTextConfirmNewPassword.setError("New password confirmation is required!");
            }

            if ((oldPassword.length() != 0) && (newPassword.length() != 0) && (confirmNewPassword.length() != 0) ) {
                updateData();
            }
        }
    }*/
}
