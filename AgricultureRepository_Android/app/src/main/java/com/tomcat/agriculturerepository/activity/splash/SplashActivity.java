/**
 * Created by Michael on 21/11/2017.
 */

package com.tomcat.agriculturerepository.activity.splash;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;

import com.tomcat.agriculturerepository.R;
import com.tomcat.agriculturerepository.activity.base.BaseActivity;
import com.tomcat.agriculturerepository.activity.login.LoginActivity;
import com.tomcat.agriculturerepository.activity.main.MainActivity;

import javax.inject.Inject;

import butterknife.ButterKnife;

public class SplashActivity extends BaseActivity implements SplashView{
    private Intent i;

    @Inject
    SplashPresenter<SplashView> presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        getActivityComponent().inject(this);

        setUnBinder(ButterKnife.bind(this));

        presenter.onAttach(SplashActivity.this);
    }

//    @Override
//    public void navigateToHome() {
//        startActivity(new Intent(this, MainActivity.class));
//        finish();
//    }
//
//    @Override
//    public void navigateToLogin() {
//        startActivity(new Intent(this, LoginActivity.class));
//        finish();
//    }
    @Override
    public void openLoginActivity() {
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {

            public void run() {
                // TODO Auto-generated method stub
                i = new Intent(SplashActivity.this, LoginActivity.class);
                startActivity(i);
                finish();
            }
        }, 1000L);
    }

    @Override
    public void openMainActivity() {
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {

            public void run() {
                // TODO Auto-generated method stub
                i = new Intent(SplashActivity.this, MainActivity.class);
                startActivity(i);
                finish();
            }
        }, 1000L);
    }

    @Override
    public void startSyncService() {
//        SyncService.start(this);
    }

    @Override
    protected void onDestroy() {
        presenter.onDetach();
        super.onDestroy();
    }

    @Override
    protected void setUp() {

    }
}
