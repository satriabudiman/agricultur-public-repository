package com.tomcat.agriculturerepository.activity.splash;

import android.content.Context;
import android.os.Handler;

import com.tomcat.agriculturerepository.R;
import com.tomcat.agriculturerepository.activity.base.BasePresenter;
import com.tomcat.agriculturerepository.activity.base.BasePresenterApp;
import com.tomcat.agriculturerepository.data.DataManager;
import com.tomcat.agriculturerepository.data.DataManagerApp;
import com.tomcat.agriculturerepository.utils.rx.SchedulerProvider;

import javax.inject.Inject;

import io.reactivex.ObservableSource;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;

/**
 * Created by albertbrucelee on 21/12/17.
 */

public class SplashPresenterApp <V extends SplashView> extends BasePresenterApp<V>
        implements SplashPresenter<V> {

    @Inject
    public SplashPresenterApp(DataManager dataManager,
                           SchedulerProvider schedulerProvider,
                           CompositeDisposable compositeDisposable) {
        super(dataManager, schedulerProvider, compositeDisposable);
    }
//
//    @Override
//    public void navigate() {
//        final Handler handler = new Handler();
//        handler.postDelayed(new Runnable() {
//            public void run() {
//                if(getDataManager().checkLoggedIn()) {
//                    getView().navigateToHome();
//                } else {
//                    getView().navigateToLogin();
//                }
//            }
//        }, 2000L);
//    }

    @Override
    public void onAttach(V mvpView) {
        super.onAttach(mvpView);

        getView().startSyncService();

//        getCompositeDisposable().add(getDataManager()
//                .seedDatabaseQuestions()
//                .subscribeOn(getSchedulerProvider().io())
//                .observeOn(getSchedulerProvider().ui())
//                .concatMap(new Function<Boolean, ObservableSource<Boolean>>() {
//                    @Override
//                    public ObservableSource<Boolean> apply(Boolean aBoolean) throws Exception {
//                        return getDataManager().seedDatabaseOptions();
//                    }
//                })
//                .subscribe(new Consumer<Boolean>() {
//                    @Override
//                    public void accept(Boolean aBoolean) throws Exception {
//                        if (!isViewAttached()) {
//                            return;
//                        }
//                        decideNextActivity();
//                    }
//                }, new Consumer<Throwable>() {
//                    @Override
//                    public void accept(Throwable throwable) throws Exception {
//                        if (!isViewAttached()) {
//                            return;
//                        }
//                        getView().onError(R.string.some_error);
//                        decideNextActivity();
//                    }
//                }));

        decideNextActivity();
    }

    private void decideNextActivity() {
        if (getDataManager().getCurrentUserLoggedInMode()
                == DataManager.LoggedInMode.LOGGED_IN_MODE_LOGGED_OUT.getType()) {
            getView().openLoginActivity();
        } else {
            getView().openMainActivity();
        }
    }
}
