package com.tomcat.agriculturerepository.data.network.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by albertbrucelee on 26/12/17.
 */

public class ProdukPenyakit {

    @Expose
    @SerializedName("id")
    private String id;

    @Expose
    @SerializedName("name")
    private String namaPenyakit;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNamaPenyakit() {
        return namaPenyakit;
    }

    public void setNamaPenyakit(String namaPenyakit) {
        this.namaPenyakit = namaPenyakit;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ProdukPenyakit)) return false;

        ProdukPenyakit produkPenyakit = (ProdukPenyakit) o;

        if (!id.equals(produkPenyakit.id)) return false;
        return namaPenyakit.equals(produkPenyakit.namaPenyakit);
    }

    @Override
    public int hashCode() {
        int result = id.hashCode();
        result = 31 * result + namaPenyakit.hashCode();
        return result;
    }
}