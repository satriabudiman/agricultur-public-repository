package com.tomcat.agriculturerepository.data.network.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by albertbrucelee on 26/12/17.
 */


public class DetailDataProduk extends DataProduk {
    @Expose
    @SerializedName("vote")
    protected List<ProdukUserVote> produkUserVote;

    @Expose
    @SerializedName("comments")
    protected List<ProdukComment> produkCommentList;


    public List<ProdukComment> getProdukCommentList() {
        return produkCommentList;
    }

    public void setProdukCommentList(List<ProdukComment> produkCommentList) {
        this.produkCommentList = produkCommentList;
    }

    public List<ProdukUserVote> getProdukUserVote() {
        return produkUserVote;
    }

    public void setProdukUserVote(List<ProdukUserVote> produkUserVote) {
        this.produkUserVote = produkUserVote;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof DetailDataProduk)) return false;

        DetailDataProduk detailDataProduk = (DetailDataProduk) o;

        if (!id.equals(detailDataProduk.id)) return false;
        if (!statusBerhasil==detailDataProduk.statusBerhasil) return false;
        if (!idPemilik.equals(detailDataProduk.idPemilik)) return false;
        if (!namaPemilik.equals(detailDataProduk.namaPemilik)) return false;
        if (!tanggalTanam.equals(detailDataProduk.tanggalTanam)) return false;
        if (!tanggalPanen.equals(detailDataProduk.tanggalPanen)) return false;
        if (!lokasi.equals(detailDataProduk.lokasi)) return false;
        if (!suhu.equals(detailDataProduk.suhu)) return false;
        if (!hargaBibit.equals(detailDataProduk.hargaBibit)) return false;
        if (!satuanHargaBibit.equals(detailDataProduk.satuanHargaBibit)) return false;
        if (!hargaJual.equals(detailDataProduk.hargaJual)) return false;
        if (!satuanHargaJual.equals(detailDataProduk.satuanHargaJual)) return false;
        if (!produkUserVote.equals(detailDataProduk.produkUserVote)) return false;
        if (!produkCommentList.equals(detailDataProduk.produkCommentList)) return false;
        if (!produkFotoList.equals(detailDataProduk.produkFotoList)) return false;
        if (!produkPenyakitList.equals(detailDataProduk.produkPenyakitList)) return false;
        return produkPupukList.equals(detailDataProduk.produkPupukList);
    }

    @Override
    public int hashCode() {
        int result = id.hashCode();
        result = 31 * result + statusBerhasil.hashCode();
        result = 31 * result + idPemilik.hashCode();
        result = 31 * result + namaPemilik.hashCode();
        result = 31 * result + tanggalTanam.hashCode();
        result = 31 * result + tanggalPanen.hashCode();
        result = 31 * result + lokasi.hashCode();
        result = 31 * result + suhu.hashCode();
        result = 31 * result + hargaBibit.hashCode();
        result = 31 * result + satuanHargaBibit.hashCode();
        result = 31 * result + hargaJual.hashCode();
        result = 31 * result + satuanHargaJual.hashCode();
        result = 31 * result + produkUserVote.hashCode();
        result = 31 * result + produkCommentList.hashCode();
        result = 31 * result + produkFotoList.hashCode();
        result = 31 * result + produkPenyakitList.hashCode();
        result = 31 * result + produkPupukList.hashCode();
        return result;
    }
}