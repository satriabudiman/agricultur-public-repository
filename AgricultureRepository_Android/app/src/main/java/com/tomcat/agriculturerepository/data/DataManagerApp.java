package com.tomcat.agriculturerepository.data;

import android.content.Context;

import com.tomcat.agriculturerepository.data.db.DbHelper;
import com.tomcat.agriculturerepository.data.db.model.User;
import com.tomcat.agriculturerepository.data.network.ApiHeader;
import com.tomcat.agriculturerepository.data.network.ApiHelper;
import com.tomcat.agriculturerepository.data.network.model.AddDataProdukResponse;
import com.tomcat.agriculturerepository.data.network.model.DataNamaProdukResponse;
import com.tomcat.agriculturerepository.data.network.model.DataProduk;
import com.tomcat.agriculturerepository.data.network.model.DataProdukByNamaRequest;
import com.tomcat.agriculturerepository.data.network.model.DataProdukByNamaResponse;
import com.tomcat.agriculturerepository.data.network.model.DeleteDataProdukRequest;
import com.tomcat.agriculturerepository.data.network.model.DeleteDataProdukResponse;
import com.tomcat.agriculturerepository.data.network.model.DetailDataProduk;
import com.tomcat.agriculturerepository.data.network.model.DetailDataProdukRequest;
import com.tomcat.agriculturerepository.data.network.model.DetailDataProdukResponse;
import com.tomcat.agriculturerepository.data.network.model.LoginRequest;
import com.tomcat.agriculturerepository.data.network.model.LoginResponse;
import com.tomcat.agriculturerepository.data.network.model.LogoutResponse;
import com.tomcat.agriculturerepository.data.network.model.UpdatePasswordRequest;
import com.tomcat.agriculturerepository.data.network.model.UpdatePasswordResponse;
import com.tomcat.agriculturerepository.data.network.model.UpdateProfileRequest;
import com.tomcat.agriculturerepository.data.network.model.UpdateProfileResponse;
import com.tomcat.agriculturerepository.data.prefs.PreferencesHelper;
import com.tomcat.agriculturerepository.di.ApplicationContext;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Observable;

/**
 * Created by albertbrucelee on 21/12/17.
 */

@Singleton
public class DataManagerApp implements DataManager{

    private Context mContext;
    private final DbHelper mDbHelper;
    private PreferencesHelper mPreferencesHelper;
    private final ApiHelper mApiHelper;

    @Inject
    public DataManagerApp(@ApplicationContext Context context,
                          DbHelper dbHelper,
                          PreferencesHelper preferencesHelper,
                          ApiHelper apiHelper) {
        mContext = context;
        mDbHelper = dbHelper;
        mPreferencesHelper = preferencesHelper;
        mApiHelper = apiHelper;
    }

    @Override
    public ApiHeader getApiHeader() {
        return mApiHelper.getApiHeader();
    }

    @Override
    public String getAccessToken() {
        return mPreferencesHelper.getAccessToken();
    }

    @Override
    public void setAccessToken(String accessToken) {
        mPreferencesHelper.setAccessToken(accessToken);
        mApiHelper.getApiHeader().getProtectedApiHeader().setAccessToken(accessToken);
    }

    @Override
    public Observable<Long> insertUser(User user) {
        return mDbHelper.insertUser(user);
    }

    @Override
    public Observable<List<User>> getAllUsers() {
        return mDbHelper.getAllUsers();
    }

    @Override
    public Observable<LoginResponse> doGoogleLoginApiCall(LoginRequest.GoogleLoginRequest
                                                                  request) {
        return mApiHelper.doGoogleLoginApiCall(request);
    }

    @Override
    public Observable<LoginResponse> doFacebookLoginApiCall(LoginRequest.FacebookLoginRequest
                                                                    request) {
        return mApiHelper.doFacebookLoginApiCall(request);
    }

    @Override
    public Observable<LoginResponse> doServerLoginApiCall(LoginRequest.ServerLoginRequest
                                                                  request) {
        return mApiHelper.doServerLoginApiCall(request);
    }

    @Override
    public Observable<UpdatePasswordResponse> doServerUpdatePasswordApiCall(UpdatePasswordRequest.ServerUpdatePasswordRequest request) {
        return mApiHelper.doServerUpdatePasswordApiCall(request);
    }

    @Override
    public Observable<UpdateProfileResponse> doServerUpdateProfileApiCall(UpdateProfileRequest.ServerUpdateProfileRequest request) {
        return mApiHelper.doServerUpdateProfileApiCall(request);
    }

    @Override
    public Observable<LogoutResponse> doLogoutApiCall() {
        return mApiHelper.doLogoutApiCall();
    }

    @Override
    public int getCurrentUserLoggedInMode() {
        return mPreferencesHelper.getCurrentUserLoggedInMode();
    }

    @Override
    public void setCurrentUserLoggedInMode(LoggedInMode mode) {
        mPreferencesHelper.setCurrentUserLoggedInMode(mode);
    }

    @Override
    public Long getCurrentUserId() {
        return mPreferencesHelper.getCurrentUserId();
    }

    @Override
    public void setCurrentUserId(Long userId) {
        mPreferencesHelper.setCurrentUserId(userId);
    }

    @Override
    public String getCurrentUserName() {
        return mPreferencesHelper.getCurrentUserName();
    }

    @Override
    public void setCurrentUserName(String userName) {
        mPreferencesHelper.setCurrentUserName(userName);
    }

    @Override
    public String getCurrentUserEmail() {
        return mPreferencesHelper.getCurrentUserEmail();
    }

    @Override
    public void setCurrentUserEmail(String email) {
        mPreferencesHelper.setCurrentUserEmail(email);
    }

    @Override
    public String getCurrentUserProfilePicUrl() {
        return mPreferencesHelper.getCurrentUserProfilePicUrl();
    }

    @Override
    public void setCurrentUserProfilePicUrl(String profilePicUrl) {
        mPreferencesHelper.setCurrentUserProfilePicUrl(profilePicUrl);
    }

    @Override
    public void updateApiHeader(Long userId, String accessToken) {
        mApiHelper.getApiHeader().getProtectedApiHeader().setUserId(userId);
        mApiHelper.getApiHeader().getProtectedApiHeader().setAccessToken(accessToken);
    }

    @Override
    public void updateUserInfo(
            String accessToken,
            Long userId,
            LoggedInMode loggedInMode,
            String userName,
            String email,
            String profilePicPath) {

        setAccessToken(accessToken);
        setCurrentUserId(userId);
        setCurrentUserLoggedInMode(loggedInMode);
        setCurrentUserName(userName);
        setCurrentUserEmail(email);
        setCurrentUserProfilePicUrl(profilePicPath);

        updateApiHeader(userId, accessToken);
    }

    @Override
    public void updatePasswordUser(String message) {

    }

    @Override
    public void updateProfileUser(String message) {

    }

    @Override
    public void setUserAsLoggedOut() {
        updateUserInfo(
                null,
                null,
                DataManager.LoggedInMode.LOGGED_IN_MODE_LOGGED_OUT,
                null,
                null,
                null);
    }


    @Override
    public Observable<DataNamaProdukResponse> getDataNamaProdukApiCall() {
        return mApiHelper.getDataNamaProdukApiCall();
    }

    @Override
    public Observable<DataProdukByNamaResponse> getDataProdukApiCall(DataProdukByNamaRequest
                                                                  request) {
        return mApiHelper.getDataProdukApiCall(request);
    }

    @Override
    public Observable<DetailDataProdukResponse> getDetailDataProdukApiCall(DetailDataProdukRequest
                                                                             request) {
        return mApiHelper.getDetailDataProdukApiCall(request);
    }

    @Override
    public Observable<AddDataProdukResponse> addDataProdukApiCall(DataProduk
                                                                                   request) {
        return mApiHelper.addDataProdukApiCall(request);
    }

    @Override
    public Observable<DeleteDataProdukResponse> deleteDataProdukApiCall(DeleteDataProdukRequest request) {
        return mApiHelper.deleteDataProdukApiCall(request);
    }

}
