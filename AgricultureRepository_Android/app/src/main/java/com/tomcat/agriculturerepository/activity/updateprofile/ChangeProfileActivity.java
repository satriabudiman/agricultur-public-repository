package com.tomcat.agriculturerepository.activity.updateprofile;

import android.Manifest;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.tomcat.agriculturerepository.R;
import com.tomcat.agriculturerepository.activity.base.BaseActivity;
import com.tomcat.agriculturerepository.activity.login.LoginActivity;
import com.tomcat.agriculturerepository.activity.main.MainActivity;
import com.tomcat.agriculturerepository.activity.updatepassword.ChangePasswordActivity;
import com.tomcat.agriculturerepository.data.db.model.User;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Michael on 26/02/2017.
 */

public class ChangeProfileActivity extends BaseActivity implements ChangeProfileView, View.OnClickListener {
    @Inject
    ChangeProfilePresenter<ChangeProfileView> presenter;

    @BindView(R.id.editTextNama)
    EditText mEditTextNama;

    @BindView(R.id.radioJK)
    RadioGroup mRadioJK;

    @BindView(R.id.radioLaki)
    RadioButton mRadioLaki;

    @BindView(R.id.radioPerempuan)
    RadioButton mRadioPerempuan;

    @BindView(R.id.editTextTanggalLahir)
    EditText mEditTextTanggalLahir;

    @BindView(R.id.editTextEmail)
    EditText mEditTextEmail;

    @BindView(R.id.buttonFoto)
    Button mButtonFoto;

    @BindView(R.id.imageViewFotoProfil)
    ImageView mImageViewFotoProfil;

    @BindView(R.id.buttonSave)
    Button mButtonSave;

    @BindView(R.id.linkPassword)
    TextView mLinkPassword;

    @BindView(R.id.progress)
    ProgressBar progressBar;

    Calendar myCalendar = Calendar.getInstance();
    private int SELECT_PICTURE = 1;
    private int SELECT_CAMERA = 1;
    private static final int REQUEST_RUNTIME_PERMISSION = 123;
    final CharSequence[] items = { "Take Photo", "Choose from Library"};
    private Uri photoPath;
    private Bitmap bitmap;

    User user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_profile);

//        ActionBar mactionBar = getSupportActionBar();
//        mactionBar.setSubtitle("Agriculture Repository");
//        mactionBar.setDisplayShowHomeEnabled(true);
//        mactionBar.setLogo(R.drawable.ic_logo);
//        mactionBar.setDisplayUseLogoEnabled(true);


//        TextSignup = (AppCompatTextView) findViewById(R.id.linkSignup);
//        TextSignup.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                startActivity(new Intent(getApplicationContext(), RegisterActivity.class));
//            }
//        });

        getActivityComponent().inject(this);

        setUnBinder(ButterKnife.bind(this));

        presenter.onAttach(this);
    }

    DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            // TODO Auto-generated method stub
            myCalendar.set(Calendar.YEAR, year);
            myCalendar.set(Calendar.MONTH, monthOfYear);
            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            updateLabel();
        }

    };

    private void updateLabel() {
        String myFormat = "yyyy-MM-dd"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
        mEditTextTanggalLahir.setText(sdf.format(myCalendar.getTime()));
    }

    @Override protected void onDestroy() {
        presenter.onDetach();
        super.onDestroy();
    }


    @Override public void updateProfileSuccess() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
        alertDialog.setTitle("Success");
        alertDialog.setMessage("Password has been changed");
        alertDialog.setNegativeButton("Close", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                finish();
            }
        });
        alertDialog.show();
    }

    @OnClick(R.id.editTextTanggalLahir)

    void showFileChooser() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)==1) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("Add Photo");
            builder.setItems(items, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int item) {
                    if (items[item].equals("Take Photo")) {

                        // you have permission go ahead
                        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        File imagesFolder = new File(Environment.getExternalStorageDirectory(), "Sports Hi");
                        imagesFolder.mkdir();

                        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(new Date());
                        File image = new File(imagesFolder, "Photo_" + timeStamp +".jpg");
                        photoPath = Uri.fromFile(image);
                        intent.putExtra(MediaStore.EXTRA_OUTPUT, photoPath);
                        startActivityForResult(intent, SELECT_CAMERA);
                    } else if (items[item].equals("Choose from Library")) {
                        Intent intent = new Intent(
                                Intent.ACTION_PICK,
                                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                        intent.setType("image/*");
                        startActivityForResult(
                                Intent.createChooser(intent, "Select File"),
                                SELECT_PICTURE);
                    }
                }
            });
            builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            });
            builder.show();
        } else {
            // you do not have permission go request runtime permissions
            ActivityCompat.requestPermissions(this, new String[] { Manifest.permission.WRITE_EXTERNAL_STORAGE }, REQUEST_RUNTIME_PERMISSION);
        }
    }

    @Override
    public void onRequestPermissionsResult(int permsRequestCode, String[] permissions, int[] grantResults) {

        switch (permsRequestCode) {

            case REQUEST_RUNTIME_PERMISSION: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // you have permission go ahead
                    AlertDialog.Builder builder = new AlertDialog.Builder(ChangeProfileActivity.this);
                    builder.setTitle("Add Photo");
                    builder.setItems(items, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int item) {
                            if (items[item].equals("Take Photo")) {

                                // you have permission go ahead
                                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                                File imagesFolder = new File(Environment.getExternalStorageDirectory(), "Sports Hi");
                                imagesFolder.mkdir();

                                String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(new Date());
                                File image = new File(imagesFolder, "Photo_" + timeStamp +".jpg");
                                photoPath = Uri.fromFile(image);
                                intent.putExtra(MediaStore.EXTRA_OUTPUT, photoPath);
                                startActivityForResult(intent, SELECT_CAMERA);
                            } else if (items[item].equals("Choose from Library")) {
                                Intent intent = new Intent(
                                        Intent.ACTION_PICK,
                                        android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                                intent.setType("image/*");
                                startActivityForResult(
                                        Intent.createChooser(intent, "Select File"),
                                        SELECT_PICTURE);
                            }
                        }
                    });
                    builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                        }
                    });
                    builder.show();
                } else {
                    // you do not have permission show toast.
                    showAlertPermission();
                }
                return;
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == SELECT_PICTURE && resultCode == RESULT_OK && data != null) {
            Uri selectedImageUri = data.getData();

            String tempPath = getPath(selectedImageUri, ChangeProfileActivity.this);
            BitmapFactory.Options btmapOptions = new BitmapFactory.Options();
            bitmap = BitmapFactory.decodeFile(tempPath, btmapOptions);
        }
        if (requestCode == SELECT_CAMERA && resultCode == RESULT_OK && data == null) {
            try {
                bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), photoPath);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        if (bitmap != null) {
            mImageViewFotoProfil.setImageBitmap(bitmap);
        }
    }

    public String getPath(Uri uri, Activity activity) {
        String[] projection = { MediaStore.MediaColumns.DATA };
        Cursor cursor = activity
                .managedQuery(uri, projection, null, null, null);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }

    public void showAlertPermission() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(ChangeProfileActivity.this);
        alertDialog.setTitle("Failed to Open Camera");
        alertDialog.setMessage("You don't have permission to take a picture. If you want to allow this permission, please allow it in settings!");
        alertDialog.setNegativeButton("Close", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        alertDialog.show();
    }

    @OnClick(R.id.buttonSave)
    @Override public void onClick(View v) {
        if(v == mEditTextTanggalLahir) {
            new DatePickerDialog(getApplicationContext(), date, myCalendar
                    .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                    myCalendar.get(Calendar.DAY_OF_MONTH)).show();
        }
        if(v == mButtonSave) {
            String JK = ((RadioButton) findViewById(mRadioJK.getCheckedRadioButtonId())).getText().toString();
            presenter.updateProfile(user.getId(), mEditTextNama.getText().toString(), JK, mEditTextTanggalLahir.getText().toString(), mEditTextEmail.getText().toString(), mImageViewFotoProfil.toString());
        }
        if(v == mButtonFoto) {
            showFileChooser();
        }
        if(v == mLinkPassword) {
            Intent intent = ChangePasswordActivity.getStartIntent(this);
            startActivity(intent);
        }
    }

    @OnClick(R.id.buttonFoto)

    @Override
    protected void setUp() {

    }


    /*private void updateData(){
        final String old_password = editTextOldPassword.getText().toString().trim();
        final String new_password = editTextNewPassword.getText().toString().trim();
        final String confirmNewPassword = editTextConfirmNewPassword.getText().toString().trim();

        if (new_password.equals(confirmNewPassword)) {

            class UpdateData extends AsyncTask<Void, Void, String> {
                ProgressDialog loading;

                @Override
                protected void onPreExecute() {
                    super.onPreExecute();
                    loading = ProgressDialog.show(ChangeProfileActivity.this, "Updating...", "Please wait...", true, false);
                }

                @Override
                protected void onPostExecute(String s) {
                    super.onPostExecute(s);
                    loading.dismiss();

                    s = s.replaceFirst(" ","");
                    s = s.trim();
                    if (s.equals("Sukses")) {
                        //Toast.makeText(getApplicationContext(), "Gagal, silahkan cek koneksi internet anda", Toast.LENGTH_LONG).show();
                        user.setPassword(new_password);
                        showAlertSuccess();
                    }
                    if (s.equals("Gagal")) {
                        //Toast.makeText(getApplicationContext(), "Gagal, silahkan cek koneksi internet anda", Toast.LENGTH_LONG).show();
                        showAlertNotValidPassword();
                    }
                }

                @Override
                protected String doInBackground(Void... params) {
                    HashMap<String, String> hashMap = new HashMap<>();
                    hashMap.put(Config.KEY_EMAIL, user.getEmail());
                    hashMap.put(Config.KEY_OLD_PASSWORD, old_password);
                    hashMap.put(Config.KEY_NEW_PASSWORD, new_password);

                    RequestHandler rh = new RequestHandler();

                    String s = rh.sendPostRequest(Config.UPDATE_PASSWORD_URL, hashMap);

                    return s;
                }
            }

            UpdateData ue = new UpdateData();
            ue.execute();
        }

        else{
            //Toast.makeText(ChangeProfileActivity.this, "Password baru dan konfirmasi password baru tidak sesuai", Toast.LENGTH_LONG).show();
            showAlertNotValid();
        }
    }

    public void showAlertSuccess() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(ChangeProfileActivity.this);
        alertDialog.setTitle("Success");
        alertDialog.setMessage("Password has been changed");
        alertDialog.setNegativeButton("Tutup",new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                finish();
            }
        });
        alertDialog.show();
    }

    public void showAlertNotValidPassword() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(ChangeProfileActivity.this);
        alertDialog.setTitle("Failed");
        alertDialog.setMessage("Incorrect current password");
        alertDialog.setNegativeButton("Tutup",new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        alertDialog.show();
    }

    public void showAlertNotValid() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(ChangeProfileActivity.this);
        alertDialog.setTitle("Tidak Sesuai");
        alertDialog.setMessage("Password baru dan konfirmasi password baru tidak sesuai");
        alertDialog.setNegativeButton("Tutup",new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        alertDialog.show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //Adding our menu to toolbar
        getMenuInflater().inflate(R.menu.menu_config, menu);
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        menu.findItem(R.id.action_refresh).setVisible(false);
        menu.findItem(R.id.action_about).setVisible(false);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onClick(View v) {
        if(v == buttonUpdate){
            oldPassword = editTextOldPassword.getText().toString();
            if(oldPassword.length() == 0) {
                editTextOldPassword.setError("Current password is required!");
            }

            newPassword = editTextNewPassword.getText().toString();
            if(newPassword.length() == 0) {
                editTextNewPassword.setError("New password is required!");
            }

            confirmNewPassword = editTextConfirmNewPassword.getText().toString();
            if(confirmNewPassword.length() == 0) {
                editTextConfirmNewPassword.setError("New password confirmation is required!");
            }

            if ((oldPassword.length() != 0) && (newPassword.length() != 0) && (confirmNewPassword.length() != 0) ) {
                updateData();
            }
        }
    }*/
}
