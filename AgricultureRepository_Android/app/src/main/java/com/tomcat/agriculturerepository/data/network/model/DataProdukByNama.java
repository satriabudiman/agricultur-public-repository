package com.tomcat.agriculturerepository.data.network.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by albertbrucelee on 26/12/17.
 */

public class DataProdukByNama extends DetailDataProduk {


    public Integer getJumlahPenyakit() {
        if(getProdukPenyakitList() !=null ) {
            return getProdukPenyakitList().size();
        }
        return null;
    }

    public Float getPersenUntung() {
        if(getHargaJual() !=null && getHargaBibit()!=null) {
            return (getHargaJual()-getHargaBibit())/getHargaBibit() * 100;
        }
        return null;
    }
    public Integer getJumlahVote() {
        if(getProdukUserVote() !=null ) {
            return getProdukUserVote().size();
        }
        return null;
    }

}