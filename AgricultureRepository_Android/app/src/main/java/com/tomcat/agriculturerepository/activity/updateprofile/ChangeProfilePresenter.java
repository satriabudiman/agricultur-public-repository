package com.tomcat.agriculturerepository.activity.updateprofile;

import com.tomcat.agriculturerepository.activity.base.BasePresenter;
import com.tomcat.agriculturerepository.di.PerActivity;

/**
 * Created by Michael on 26/02/2017.
 */

@PerActivity
public interface ChangeProfilePresenter<V extends ChangeProfileView> extends BasePresenter<V> {

    void updateProfile(Long id, String name, String jenisKelamin, String tanggalLahir, String email, String profilePhoto);

}