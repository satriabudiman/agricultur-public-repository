package com.tomcat.agriculturerepository.activity.updateprofile;

import com.androidnetworking.error.ANError;
import com.tomcat.agriculturerepository.R;
import com.tomcat.agriculturerepository.activity.base.BasePresenterApp;
import com.tomcat.agriculturerepository.data.DataManager;
import com.tomcat.agriculturerepository.data.network.model.UpdatePasswordRequest;
import com.tomcat.agriculturerepository.data.network.model.UpdatePasswordResponse;
import com.tomcat.agriculturerepository.data.network.model.UpdateProfileRequest;
import com.tomcat.agriculturerepository.data.network.model.UpdateProfileResponse;
import com.tomcat.agriculturerepository.utils.rx.SchedulerProvider;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;

/**
 * Created by Michael on 26/02/2017.
 */

public class ChangeProfilePresenterApp<V extends ChangeProfileView> extends BasePresenterApp<V>
        implements ChangeProfilePresenter<V> {

    @Inject
    public ChangeProfilePresenterApp(DataManager dataManager,
                             SchedulerProvider schedulerProvider,
                             CompositeDisposable compositeDisposable) {
        super(dataManager, schedulerProvider, compositeDisposable);
    }

//    public LoginPresenterApp(Context context){
//        super(context);
//    }

    @Override public void updateProfile(Long id, String name, String jenisKelamin, String tanggalLahir, String email, String profilePhoto) {
        //validate email and password
        if (id == null) {
            getView().onError("Please provide a non empty id");
            return;
        }
        if (jenisKelamin == null || jenisKelamin.isEmpty()) {
            getView().onError("Please provide a non empty gender");
            return;
        }
        if (tanggalLahir == null || tanggalLahir.isEmpty()) {
            getView().onError("Please provide a non empty birth date");
            return;
        }
        if (email == null || email.isEmpty()) {
            getView().onError(R.string.empty_email);
            return;
        }
        getView().showLoading();

        getCompositeDisposable().add(getDataManager()
                .doServerUpdateProfileApiCall(new UpdateProfileRequest.ServerUpdateProfileRequest(id, name, jenisKelamin, tanggalLahir, email, profilePhoto))
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(new Consumer<UpdateProfileResponse>() {
                    @Override
                    public void accept(UpdateProfileResponse response) throws Exception {
                        getDataManager().updateProfileUser(
                                response.getMessage());

                        if (!isViewAttached()) {
                            return;
                        }

                        getView().hideLoading();
                        getView().updateProfileSuccess();

                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {

                        if (!isViewAttached()) {
                            return;
                        }

                        getView().hideLoading();

                        // handle the login error here
                        if (throwable instanceof ANError) {
                            ANError anError = (ANError) throwable;
                            handleApiError(anError);
                        }
                    }
                }));

    }
}
