/*
 * Copyright (C) 2017 MINDORKS NEXTGEN PRIVATE LIMITED
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://mindorks.com/license/apache-v2
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */

package com.tomcat.agriculturerepository.activity.detaildataproduk;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.tomcat.agriculturerepository.R;
import com.tomcat.agriculturerepository.activity.base.BaseViewHolder;
import com.tomcat.agriculturerepository.data.network.model.ProdukComment;
import com.tomcat.agriculturerepository.data.network.model.ProdukFoto;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Janisharali on 25-05-2017.
 */

public class PhotoProdukAdapter extends RecyclerView.Adapter<BaseViewHolder> {

    public static final int VIEW_TYPE_EMPTY = 0;
    public static final int VIEW_TYPE_NORMAL = 1;

    private Callback mCallback;
    private List<ProdukFoto> mProdukPhotoList;

    public PhotoProdukAdapter(List<ProdukFoto> produkPhotoList) {
        mProdukPhotoList = produkPhotoList;
    }

    public void setCallback(Callback callback) {
        mCallback = callback;
    }

    @Override
    public void onBindViewHolder(BaseViewHolder holder, int position) {
        holder.onBind(position);
    }

    @Override
    public BaseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        switch (viewType) {
            case VIEW_TYPE_NORMAL:
                return new ViewHolder(
                        LayoutInflater.from(parent.getContext()).inflate(R.layout.item_detail_data_produk_photo_view, parent, false));
            case VIEW_TYPE_EMPTY:
            default:
                return new EmptyViewHolder(
                        LayoutInflater.from(parent.getContext()).inflate(R.layout.item_empty_view, parent, false));
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (mProdukPhotoList != null && mProdukPhotoList.size() > 0) {
            return VIEW_TYPE_NORMAL;
        } else {
            return VIEW_TYPE_EMPTY;
        }
    }

    @Override
    public int getItemCount() {
        if (mProdukPhotoList != null && mProdukPhotoList.size() > 0) {
            return mProdukPhotoList.size();
        } else {
            return 1;
        }
    }

    public void addItems(List<ProdukFoto> produkFotoList) {
        mProdukPhotoList.addAll(produkFotoList);
        notifyDataSetChanged();
    }

    public void clearItems() {
        mProdukPhotoList.clear();
        notifyDataSetChanged();
    }

    public interface Callback {
        void onEmptyViewRetryClick();
    }

    public class ViewHolder extends BaseViewHolder {

        @BindView(R.id.detail_produk_photo_image_view)
        ImageView photoImageView;


        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        protected void clear() {
            photoImageView.setImageDrawable(null);
        }

        public void onBind(int position) {
            super.onBind(position);

            final ProdukFoto produkFoto = mProdukPhotoList.get(position);

            if (produkFoto.getUrl() != null) {
                Glide.with(itemView.getContext())
                        .load(produkFoto.getUrl())
                        .asBitmap()
                        .centerCrop()
                        .into(photoImageView);
            }
        }
    }

    public class EmptyViewHolder extends BaseViewHolder {

        @BindView(R.id.btn_retry)
        Button retryButton;

        @BindView(R.id.tv_message)
        TextView messageTextView;

        public EmptyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @Override
        protected void clear() {

        }

        @OnClick(R.id.btn_retry)
        void onRetryClick() {
            if (mCallback != null)
                mCallback.onEmptyViewRetryClick();
        }
    }
}
