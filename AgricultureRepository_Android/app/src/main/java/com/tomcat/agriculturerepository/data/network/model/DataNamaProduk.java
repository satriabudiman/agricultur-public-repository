package com.tomcat.agriculturerepository.data.network.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by albertbrucelee on 26/12/17.
 */

public class DataNamaProduk {
    @Expose
    @SerializedName("id")
    private String id;

    @Expose
    @SerializedName("foto_produk")
    private String fotoProduk;

    @Expose
    @SerializedName("name")
    private String namaProduk;

    @Expose
    @SerializedName("jumlah_data")
    private String jumlahData;

    @Expose
    @SerializedName("persen_berhasil")
    private String persenBerhasil;

    @Expose
    @SerializedName("persen_untung")
    private String persenUntung;

    @Expose
    @SerializedName("tahun_produk_pertama")
    private String tahunProdukPertama;

    @Expose
    @SerializedName("tahun_produk_terakhir")
    private String tahunProdukTerakhir;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setFotoProduk(String namaProduk) {
        this.fotoProduk = fotoProduk;
    }

    public void setNamaProduk(String namaProduk) {
        this.namaProduk = namaProduk;
    }

    public String getFotoProduk() {
        return fotoProduk;
    }

    public String getNamaProduk() {
        return namaProduk;
    }

    public String getJumlahData() {
        return jumlahData;
    }

    public void setJumlahData(String jumlahData) {
        this.jumlahData = jumlahData;
    }

    public String getPersenBerhasil() {
        return persenBerhasil;
    }

    public void setPersenBerhasil(String persenBerhasil) {
        this.persenBerhasil = persenBerhasil;
    }

    public String getPersenUntung() {
        return persenUntung;
    }

    public void setPersenUntung(String persenUntung) {
        this.persenUntung = persenUntung;
    }

    public String getTahunProdukPertama() {
        return tahunProdukPertama;
    }

    public void setTahunProdukPertama(String tahunProdukPertama) {
        this.tahunProdukPertama = tahunProdukPertama;
    }

    public String getTahunProdukTerakhir() {
        return tahunProdukTerakhir;
    }

    public void setTahunProdukTerakhir(String tahunProdukTerakhir) {
        this.tahunProdukTerakhir = tahunProdukTerakhir;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof DataNamaProduk)) return false;

        DataNamaProduk dataNamaProduk = (DataNamaProduk) o;

        if (!id.equals(dataNamaProduk.id)) return false;
        if (!id.equals(dataNamaProduk.fotoProduk)) return false;
        if (!namaProduk.equals(dataNamaProduk.namaProduk)) return false;
        if (!jumlahData.equals(dataNamaProduk.jumlahData)) return false;
        if (!persenBerhasil.equals(dataNamaProduk.persenBerhasil)) return false;
        if (!persenUntung.equals(dataNamaProduk.persenUntung)) return false;
        if (!tahunProdukPertama.equals(dataNamaProduk.tahunProdukPertama)) return false;
        return tahunProdukTerakhir.equals(dataNamaProduk.tahunProdukTerakhir);
    }

    @Override
    public int hashCode() {
        int result = id.hashCode();
        result = 31 * result + fotoProduk.hashCode();
        result = 31 * result + namaProduk.hashCode();
        result = 31 * result + jumlahData.hashCode();
        result = 31 * result + persenBerhasil.hashCode();
        result = 31 * result + persenUntung.hashCode();
        result = 31 * result + tahunProdukPertama.hashCode();
        result = 31 * result + tahunProdukTerakhir.hashCode();
        return result;
    }
}
