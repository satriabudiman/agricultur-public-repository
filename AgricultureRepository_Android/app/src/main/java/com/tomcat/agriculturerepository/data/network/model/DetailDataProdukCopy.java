package com.tomcat.agriculturerepository.data.network.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by albertbrucelee on 26/12/17.
 */


public class DetailDataProdukCopy {
    @Expose
    @SerializedName("id")
    private String id;

    @Expose
    @SerializedName("category")
    private KategoriNamaProduk kategoriNamaProduk;

    @Expose
    @SerializedName("status")
    private Boolean statusBerhasil;

    @Expose
    @SerializedName("idPemilik")
    private String idPemilik;

    @Expose
    @SerializedName("namaPemilik")
    private String namaPemilik;

    @Expose
    @SerializedName("tanggalTanam")
    private String tanggalTanam;

    @Expose
    @SerializedName("tanggalPanen")
    private String tanggalPanen;

    @Expose
    @SerializedName("luasLahan")
    private String luasLahan;

    @Expose
    @SerializedName("lokasi")
    private String lokasi;

    @Expose
    @SerializedName("cuaca")
    private String cuaca;

    @Expose
    @SerializedName("suhu")
    private String suhu;

    @Expose
    @SerializedName("modal")
    private String hargaBibit;

    @Expose
    @SerializedName("satuanHargaBibit")
    private String satuanHargaBibit;

    @Expose
    @SerializedName("harga")
    private String hargaJual;

    @Expose
    @SerializedName("satuanHargaJual")
    private String satuanHargaJual;
//
    @Expose
    @SerializedName("photos")
    private List<ProdukFoto> produkFotoList;

    @Expose
    @SerializedName("diseases")
    private List<ProdukPenyakit> produkPenyakitList;

    @Expose
    @SerializedName("pupuk")
    private List<ProdukPupuk> produkPupukList;

    @Expose
    @SerializedName("vote")
    private Integer produkVote;

    @Expose
    @SerializedName("comments")
    private List<ProdukComment> produkCommentList;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public KategoriNamaProduk getKategoriNamaProduk() {
        return kategoriNamaProduk;
    }

    public void setKategoriNamaProduk(KategoriNamaProduk kategoriNamaProduk) {
        this.kategoriNamaProduk = kategoriNamaProduk;
    }

    public boolean getStatusBerhasil() {
        return statusBerhasil;
    }

    public void setStatusBerhasil(boolean statusBerhasil) {
        this.statusBerhasil = statusBerhasil;
    }

    public String getIdPemilik() {
        return idPemilik;
    }

    public void setIdPemilik(String idPemilik) {
        this.idPemilik = idPemilik;
    }

    public String getNamaPemilik() {
        return namaPemilik;
    }

    public void setNamaPemilik(String namaPemilik) {
        this.namaPemilik = namaPemilik;
    }

    public String getTanggalTanam() {
        return tanggalTanam;
    }

    public void setTanggalTanam(String tanggalTanam) {
        this.tanggalTanam = tanggalTanam;
    }

    public String getTanggalPanen() {
        return tanggalPanen;
    }

    public void setTanggalPanen(String tanggalPanen) {
        this.tanggalPanen = tanggalPanen;
    }

    public String getLuasLahan() {
        return luasLahan;
    }

    public void setLuasLahan(String luasLahan) {
        this.luasLahan = luasLahan;
    }

    public String getLokasi() {
        return lokasi;
    }

    public void setLokasi(String lokasi) {
        this.lokasi = lokasi;
    }

    public String getCuaca() {
        return cuaca;
    }

    public void setCuaca(String cuaca) {
        this.cuaca = cuaca;
    }

    public String getSuhu() {
        return suhu;
    }

    public void setSuhu(String suhu) {
        this.suhu = suhu;
    }

    public String getHargaBibit() {
        return hargaBibit;
    }

    public void setHargaBibit(String hargaBibit) {
        this.hargaBibit = hargaBibit;
    }

    public String getSatuanHargaBibit() {
        return satuanHargaBibit;
    }

    public void setSatuanHargaBibit(String satuanHargaBibit) {
        this.satuanHargaBibit = satuanHargaBibit;
    }

    public String getHargaJual() {
        return hargaJual;
    }

    public void setHargaJual(String hargaJual) {
        this.hargaJual = hargaJual;
    }

    public String getSatuanHargaJual() {
        return satuanHargaJual;
    }

    public void setSatuanHargaJual(String satuanHargaJual) {
        this.satuanHargaJual = satuanHargaJual;
    }

    public List<ProdukFoto> getProdukFotoList() {
        return produkFotoList;
    }

    public void setProdukFotoList(List<ProdukFoto> produkFotoList) {
        this.produkFotoList = produkFotoList;
    }

    public List<ProdukPenyakit> getProdukPenyakitList() {
        return produkPenyakitList;
    }

    public void setProdukPenyakitList(List<ProdukPenyakit> produkPenyakitList) {
        this.produkPenyakitList = produkPenyakitList;
    }

    public List<ProdukPupuk> getProdukPupukList() {
        return produkPupukList;
    }

    public void setProdukPupukList(List<ProdukPupuk> produkPupukList) {
        this.produkPupukList = produkPupukList;
    }


    public List<ProdukComment> getProdukCommentList() {
        return produkCommentList;
    }

    public void setProdukCommentList(List<ProdukComment> produkCommentList) {
        this.produkCommentList = produkCommentList;
    }

    public void setStatusBerhasil(Boolean statusBerhasil) {
        this.statusBerhasil = statusBerhasil;
    }

    public int getProdukVote() {
        return produkVote;
    }

    public void setProdukVote(int produkVote) {
        this.produkVote = produkVote;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof DetailDataProdukCopy)) return false;

        DetailDataProdukCopy detailDataProduk = (DetailDataProdukCopy) o;

        if (!id.equals(detailDataProduk.id)) return false;
        if (!statusBerhasil==detailDataProduk.statusBerhasil) return false;
        if (!idPemilik.equals(detailDataProduk.idPemilik)) return false;
        if (!namaPemilik.equals(detailDataProduk.namaPemilik)) return false;
        if (!tanggalTanam.equals(detailDataProduk.tanggalTanam)) return false;
        if (!tanggalPanen.equals(detailDataProduk.tanggalPanen)) return false;
        if (!lokasi.equals(detailDataProduk.lokasi)) return false;
        if (!cuaca.equals(detailDataProduk.cuaca)) return false;
        if (!suhu.equals(detailDataProduk.suhu)) return false;
        if (!hargaBibit.equals(detailDataProduk.hargaBibit)) return false;
        if (!satuanHargaBibit.equals(detailDataProduk.satuanHargaBibit)) return false;
        if (!hargaJual.equals(detailDataProduk.hargaJual)) return false;
        if (!satuanHargaJual.equals(detailDataProduk.satuanHargaJual)) return false;
        if (!produkVote.equals(detailDataProduk.produkVote)) return false;
        if (!produkCommentList.equals(detailDataProduk.produkCommentList)) return false;
        if (!produkFotoList.equals(detailDataProduk.produkFotoList)) return false;
        if (!produkPenyakitList.equals(detailDataProduk.produkPenyakitList)) return false;
        return produkPupukList.equals(detailDataProduk.produkPupukList);
    }

    @Override
    public int hashCode() {
        int result = id.hashCode();
        result = 31 * result + statusBerhasil.hashCode();
        result = 31 * result + idPemilik.hashCode();
        result = 31 * result + namaPemilik.hashCode();
        result = 31 * result + tanggalTanam.hashCode();
        result = 31 * result + tanggalPanen.hashCode();
        result = 31 * result + lokasi.hashCode();
        result = 31 * result + cuaca.hashCode();
        result = 31 * result + suhu.hashCode();
        result = 31 * result + hargaBibit.hashCode();
        result = 31 * result + satuanHargaBibit.hashCode();
        result = 31 * result + hargaJual.hashCode();
        result = 31 * result + satuanHargaJual.hashCode();
        result = 31 * result + produkVote.hashCode();
        result = 31 * result + produkCommentList.hashCode();
        result = 31 * result + produkFotoList.hashCode();
        result = 31 * result + produkPenyakitList.hashCode();
        result = 31 * result + produkPupukList.hashCode();
        return result;
    }
}