package com.tomcat.agriculturerepository.activity.updatepassword;

import com.tomcat.agriculturerepository.activity.base.BaseView;

import java.util.List;

/**
 * Created by Michael on 26/02/2017.
 */

public interface ChangePasswordView extends BaseView {
    void updatePasswordSuccess();
}
