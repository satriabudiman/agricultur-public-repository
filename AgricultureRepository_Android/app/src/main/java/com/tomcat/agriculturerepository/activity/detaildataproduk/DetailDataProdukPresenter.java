/*
 * Copyright (C) 2017 MINDORKS NEXTGEN PRIVATE LIMITED
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://mindorks.com/license/apache-v2
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */

package com.tomcat.agriculturerepository.activity.detaildataproduk;

import com.tomcat.agriculturerepository.activity.base.BasePresenter;

/**
 * Created by janisharali on 25/05/17.
 */

public interface DetailDataProdukPresenter<V extends DetailDataProdukView>
        extends BasePresenter<V> {

    void setIdDataProduk(int idDataProduk);

    void onViewPrepared();

    void deleteProduk();
}


