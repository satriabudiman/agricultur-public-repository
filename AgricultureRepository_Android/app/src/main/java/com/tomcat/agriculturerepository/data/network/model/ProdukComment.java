package com.tomcat.agriculturerepository.data.network.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by albertbrucelee on 26/12/17.
 */

public class ProdukComment {

    @Expose
    @SerializedName("id")
    private String id;

    @Expose
    @SerializedName("nama_user")
    private String namaUser;

    @Expose
    @SerializedName("komentar")
    private String komentar;

    @Expose
    @SerializedName("tanggal")
    private String tanggal;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNamaUser() {
        return namaUser;
    }

    public void setNamaUser(String namaUser) {
        this.namaUser = namaUser;
    }

    public void setKomentar(String komentar) {
        this.komentar = komentar;
    }

    public String getKomentar() {
        return komentar;
    }

    public String getTanggal() {
        return tanggal;
    }

    public void setTanggal(String tanggal) {
        this.tanggal = tanggal;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ProdukComment)) return false;

        ProdukComment produkComment = (ProdukComment) o;

        if (!id.equals(produkComment.id)) return false;
        if (!namaUser.equals(produkComment.namaUser)) return false;
        if (!komentar.equals(produkComment.komentar)) return false;
        return tanggal.equals(produkComment.tanggal);
    }

    @Override
    public int hashCode() {
        int result = id.hashCode();
        result = 31 * result + namaUser.hashCode();
        result = 31 * result + komentar.hashCode();
        result = 31 * result + tanggal.hashCode();
        return result;
    }
}