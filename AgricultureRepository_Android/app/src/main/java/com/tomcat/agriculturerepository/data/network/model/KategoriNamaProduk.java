package com.tomcat.agriculturerepository.data.network.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by albertbrucelee on 26/12/17.
 */

public class KategoriNamaProduk {
    @Expose
    @SerializedName("id")
    private Integer id;

    @Expose
    @SerializedName("name")
    private String namaProduk;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setNamaProduk(String namaProduk) {
        this.namaProduk = namaProduk;
    }

    public String getNamaProduk() {
        return namaProduk;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof DataNamaProduk)) return false;

        KategoriNamaProduk kategoriNamaProduk = (KategoriNamaProduk) o;

        if (!id.equals(kategoriNamaProduk.id)) return false;
        return kategoriNamaProduk.equals(kategoriNamaProduk.namaProduk);
    }

    @Override
    public int hashCode() {
        int result = id.hashCode();
        result = 31 * result + namaProduk.hashCode();
        return result;
    }
}
