/*
 * Copyright (C) 2017 MINDORKS NEXTGEN PRIVATE LIMITED
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://mindorks.com/license/apache-v2
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */

package com.tomcat.agriculturerepository.activity.main;

import android.util.Log;

import com.androidnetworking.error.ANError;
import com.tomcat.agriculturerepository.activity.base.BasePresenterApp;
import com.tomcat.agriculturerepository.data.DataManager;
import com.tomcat.agriculturerepository.data.network.model.DataNamaProdukResponse;
import com.tomcat.agriculturerepository.data.network.model.LogoutResponse;
import com.tomcat.agriculturerepository.utils.rx.SchedulerProvider;

import java.io.Console;

import javax.inject.Inject;

import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;


/**
 * Created by janisharali on 27/01/17.
 */

public class MainPresenterApp<V extends MainView> extends BasePresenterApp<V>
        implements MainPresenter<V> {

    private static final String TAG = "MainPresenterApp";

    @Inject
    public MainPresenterApp(DataManager dataManager,
                            SchedulerProvider schedulerProvider,
                            CompositeDisposable compositeDisposable) {
        super(dataManager, schedulerProvider, compositeDisposable);
    }

    @Override
    public void onDrawerOptionAboutClick() {
        getView().closeNavigationDrawer();
        getView().showAboutFragment();
    }

    @Override
    public void onDrawerOptionLogoutClick() {
        getView().showLoading();

        getCompositeDisposable().add(getDataManager().doLogoutApiCall()
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(new Consumer<LogoutResponse>() {
                    @Override
                    public void accept(LogoutResponse response) throws Exception {
                        if (!isViewAttached()) {
                            return;
                        }

                        getDataManager().setUserAsLoggedOut();
                        getView().hideLoading();
                        getView().openLoginActivity();
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        if (!isViewAttached()) {
                            return;
                        }

                        getView().hideLoading();

                        // handle the login error here
                        if (throwable instanceof ANError) {
                            ANError anError = (ANError) throwable;
                            handleApiError(anError);
                        }
                    }
                }));

    }

    @Override
    public void onViewInitialized() {
//        getCompositeDisposable().add(getDataManager()
//                .getAllQuestions()
//                .subscribeOn(getSchedulerProvider().io())
//                .observeOn(getSchedulerProvider().ui())
//                .subscribe(new Consumer<List<Question>>() {
//                    @Override
//                    public void accept(List<Question> questionList) throws Exception {
//                        if (!isViewAttached()) {
//                            return;
//                        }
//
//                        if (questionList != null) {
//                            getView().refreshQuestionnaire(questionList);
//                        }
//                    }
//                }));
    }

    @Override
    public void onCardExhausted() {
//        getCompositeDisposable().add(getDataManager()
//                .getAllQuestions()
//                .subscribeOn(getSchedulerProvider().io())
//                .observeOn(getSchedulerProvider().ui())
//                .subscribe(new Consumer<List<Question>>() {
//                    @Override
//                    public void accept(List<Question> questionList) throws Exception {
//                        if (!isViewAttached()) {
//                            return;
//                        }
//
//                        if (questionList != null) {
//                            getView().reloadQuestionnaire(questionList);
//                        }
//                    }
//                }));
    }

    @Override
    public void onNavMenuCreated() {
        if (!isViewAttached()) {
            return;
        }
        getView().updateAppVersion();

        final String currentUserName = getDataManager().getCurrentUserName();
        if (currentUserName != null && !currentUserName.isEmpty()) {
            getView().updateUserName(currentUserName);
        }

        final String currentUserEmail = getDataManager().getCurrentUserEmail();
        //Log.d("EMAILLL",currentUserEmail);
        if (currentUserEmail != null && !currentUserEmail.isEmpty()) {
            getView().updateUserEmail(currentUserEmail);
        }

        final String profilePicUrl = getDataManager().getCurrentUserProfilePicUrl();
        if (profilePicUrl != null && !profilePicUrl.isEmpty()) {
            getView().updateUserProfilePic(profilePicUrl);
        }
    }

    @Override
    public void onViewPrepared() {
        getView().showLoading();
        getCompositeDisposable().add(getDataManager()
                .getDataNamaProdukApiCall()
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(new Consumer<DataNamaProdukResponse>() {
                    @Override
                    public void accept(@NonNull DataNamaProdukResponse dataNamaProdukResponse)
                            throws Exception {
                        if (dataNamaProdukResponse != null && dataNamaProdukResponse.getData() != null) {
                            getView().updateDataNamaProduk(dataNamaProdukResponse.getData());
                        }
                        getView().hideLoading();
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(@NonNull Throwable throwable)
                            throws Exception {
                        if (!isViewAttached()) {
                            return;
                        }

                        getView().hideLoading();

                        // handle the error here
                        if (throwable instanceof ANError) {
                            ANError anError = (ANError) throwable;
                            handleApiError(anError);
                        }
                    }
                }));
    }


//    @Override
//    public void onDrawerRateUsClick() {
//        getView().closeNavigationDrawer();
//        getView().showRateUsDialog();
//    }
//
//    @Override
//    public void onDrawerMyFeedClick() {
//        getView().closeNavigationDrawer();
//        getView().openMyFeedActivity();
//    }
}
