/*
 * Copyright (C) 2017 MINDORKS NEXTGEN PRIVATE LIMITED
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://mindorks.com/license/apache-v2
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */

package com.tomcat.agriculturerepository.activity.detaildataproduk;

import com.tomcat.agriculturerepository.activity.base.BaseView;
import com.tomcat.agriculturerepository.data.network.model.DetailDataProduk;
import com.tomcat.agriculturerepository.data.network.model.ProdukComment;
import com.tomcat.agriculturerepository.data.network.model.ProdukFoto;
import com.tomcat.agriculturerepository.data.network.model.ProdukPenyakit;
import com.tomcat.agriculturerepository.data.network.model.ProdukPupuk;
import com.tomcat.agriculturerepository.data.network.model.ProdukUserVote;

import java.util.List;

/**
 * Created by janisharali on 25/05/17.
 */

public interface DetailDataProdukView extends BaseView {

    void updateDetailDataProduk(DetailDataProduk detailDataProduk);
    void updateProdukPupuk(List<ProdukPupuk> produkPupukList);
    void updateProdukPenyakit(List<ProdukPenyakit> produkPenyakitList);
    void updateProdukFoto(List<ProdukFoto> produkFotoList);
    void updateProdukUserVote(List<ProdukUserVote> produkUserVotes);
    void updateProdukComment(List<ProdukComment> produkCommentList);

    void navigateDoneDeleteActivity();
}
