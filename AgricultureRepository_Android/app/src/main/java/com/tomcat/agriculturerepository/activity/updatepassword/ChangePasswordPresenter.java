package com.tomcat.agriculturerepository.activity.updatepassword;

import com.tomcat.agriculturerepository.activity.base.BasePresenter;
import com.tomcat.agriculturerepository.di.PerActivity;

import java.util.HashMap;

/**
 * Created by Michael on 26/02/2017.
 */

@PerActivity
public interface ChangePasswordPresenter<V extends ChangePasswordView> extends BasePresenter<V> {

    void updatePassword(Long id, String oldPassword, String newPassword);

//    void onUsernameError();
//
//    void onPasswordError();
//
//    void onSuccess();

}