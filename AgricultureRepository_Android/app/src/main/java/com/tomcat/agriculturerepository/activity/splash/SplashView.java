package com.tomcat.agriculturerepository.activity.splash;

import com.tomcat.agriculturerepository.activity.base.BaseView;

/**
 * Created by albertbrucelee on 21/12/17.
 */

public interface SplashView extends BaseView {

    void openLoginActivity();

    void openMainActivity();

    void startSyncService();

}
