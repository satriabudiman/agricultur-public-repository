/*
 * Copyright (C) 2017 MINDORKS NEXTGEN PRIVATE LIMITED
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://mindorks.com/license/apache-v2
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */

package com.tomcat.agriculturerepository.activity.detaildataproduk;

import android.graphics.drawable.Animatable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.tomcat.agriculturerepository.R;
import com.tomcat.agriculturerepository.activity.about.AboutFragment;
import com.tomcat.agriculturerepository.activity.base.BaseFragment;
import com.tomcat.agriculturerepository.activity.main.MainActivity;
import com.tomcat.agriculturerepository.data.network.model.DetailDataProduk;
import com.tomcat.agriculturerepository.data.network.model.ProdukComment;
import com.tomcat.agriculturerepository.data.network.model.ProdukFoto;
import com.tomcat.agriculturerepository.data.network.model.ProdukPenyakit;
import com.tomcat.agriculturerepository.data.network.model.ProdukPupuk;
import com.tomcat.agriculturerepository.data.network.model.ProdukUserVote;
import com.tomcat.agriculturerepository.di.component.ActivityComponent;

import java.io.Console;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by janisharali on 25/05/17.
 */

public class DetailDataProdukFragment extends BaseFragment implements
        DetailDataProdukView, KomentarProdukAdapter.Callback, PhotoProdukAdapter.Callback  {

    public static final String TAG = "DetailDataProdukFragment";

    public static final String TAG_PASS_ID_DATA_PRODUK = "id_data_produk";

    @BindView(R.id.toolbar)
    Toolbar mToolbar;

    @Inject
    DetailDataProdukPresenter<DetailDataProdukView> mPresenter;


    @Inject
    KomentarProdukAdapter mKomentarProdukAdapter;

    @Inject
    PhotoProdukAdapter mPhotoProdukAdapter;

    @Inject
    LinearLayoutManager mLayoutManagerProdukComment;

    @Inject
    LinearLayoutManager mLayoutManagerProdukPhoto;

    @BindView(R.id.detail_data_produk_comment_recycler_view)
    RecyclerView mProdukCommentRecyclerView;

    @BindView(R.id.detail_data_produk_photo_recycler_view)
    RecyclerView mProdukPhotoRecyclerView;

    @BindView(R.id.status_berhasil_text_view)
    TextView statusBerhasilTextView;

    @BindView(R.id.nama_pemilik_text_view)
    TextView namaPemilikTextView;

    @BindView(R.id.jumlah_vote_text_view)
    TextView jumlahVoteTextView;

    @BindView(R.id.penyakit_text_view)
    TextView penyakitTextView;

    @BindView(R.id.pupuk_text_view)
    TextView pupukTextView;

    @BindView(R.id.lokasi_text_view)
    TextView lokasiTextView;

    @BindView(R.id.luas_lahan_text_view)
    TextView luasLahanTextView;

    @BindView(R.id.cuaca_text_view)
    TextView cuacaTextView;

    @BindView(R.id.suhu_text_view)
    TextView suhuTextView;

    @BindView(R.id.tanggal_tanam_text_view)
    TextView tanggalTanamTextView;

    @BindView(R.id.tanggal_panen_text_view)
    TextView tanggalPenenTextView;

    @BindView(R.id.harga_bibit_text_view)
    TextView hargaBibitTextView;

    @BindView(R.id.satuan_harga_bibit_text_view)
    TextView satuanHargaBibitTextView;

    @BindView(R.id.harga_jual_text_view)
    TextView satuanHargaTextView;

    @BindView(R.id.satuan_harga_jual_text_view)
    TextView satuanHargaJualTextView;


    public static DetailDataProdukFragment newInstance(int idDetailDataProduk) {
        Bundle args = new Bundle();
        args.putInt(TAG_PASS_ID_DATA_PRODUK, idDetailDataProduk);
        DetailDataProdukFragment fragment = new DetailDataProdukFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_detail_data_produk, container, false);

        ActivityComponent component = getActivityComponent();
        if (component != null) {
            component.inject(this);
            setUnBinder(ButterKnife.bind(this, view));
            mPresenter.onAttach(this);
            mPresenter.setIdDataProduk(this.getArguments().getInt(TAG_PASS_ID_DATA_PRODUK));
            mKomentarProdukAdapter.setCallback(this);
            mPhotoProdukAdapter.setCallback(this);
        }

        ((AppCompatActivity)getActivity()).setSupportActionBar(mToolbar);
        ((MainActivity)getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setHasOptionsMenu(true);

        return view;
    }

    @Override
    protected void setUp(View view) {
        //set this to avoid clicked parent if we click anything on this fragment view
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            }
        });

        mLayoutManagerProdukComment.setOrientation(LinearLayoutManager.VERTICAL);
        mProdukCommentRecyclerView.setLayoutManager(mLayoutManagerProdukComment);
        mProdukCommentRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mProdukCommentRecyclerView.setAdapter(mKomentarProdukAdapter);

        mLayoutManagerProdukPhoto.setOrientation(LinearLayoutManager.HORIZONTAL);
        mProdukPhotoRecyclerView.setLayoutManager(mLayoutManagerProdukPhoto);
        mProdukPhotoRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mProdukPhotoRecyclerView.setAdapter(mPhotoProdukAdapter);

        mPresenter.onViewPrepared();
    }

    @Override
    public void onEmptyViewRetryClick() {
        mKomentarProdukAdapter.clearItems();
        mKomentarProdukAdapter.clearItems();
        mPresenter.onViewPrepared();
    }


    @Override
    public void updateDetailDataProduk(DetailDataProduk detailDataProduk) {
        if (detailDataProduk.getStatusBerhasil()!=null) {
            if(detailDataProduk.getStatusBerhasil()) {
                statusBerhasilTextView.setText(R.string.text_status_berhasil);
            } else {
                statusBerhasilTextView.setText(R.string.text_status_gagal);
            }
        }
        if (detailDataProduk.getNamaPemilik() != null) {
            namaPemilikTextView.setText(detailDataProduk.getNamaPemilik());
        }
        if (detailDataProduk.getLokasi() != null) {
            lokasiTextView.setText(detailDataProduk.getLokasi());
        }
        if (detailDataProduk.getLuasLahan() != null) {
            luasLahanTextView.setText(String.valueOf(detailDataProduk.getLuasLahan()));
        }
        if (detailDataProduk.getSuhu() != null) {
            suhuTextView.setText(detailDataProduk.getSuhu());
        }
        if (detailDataProduk.getTanggalTanam() != null) {
            tanggalTanamTextView.setText(detailDataProduk.getTanggalTanam());
        }
        if (detailDataProduk.getTanggalPanen() != null) {
            tanggalPenenTextView.setText(detailDataProduk.getTanggalPanen());
        }
        if (detailDataProduk.getHargaBibit() != null) {
            hargaBibitTextView.setText(String.valueOf(detailDataProduk.getHargaBibit()));
        }
        if (detailDataProduk.getSatuanHargaBibit() != null) {
            satuanHargaBibitTextView.setText(detailDataProduk.getSatuanHargaBibit().getNama());
        }
        if (detailDataProduk.getHargaJual() != null) {
            satuanHargaTextView.setText(String.valueOf(detailDataProduk.getHargaJual()));
        }
        if (detailDataProduk.getSatuanHargaJual() != null) {
            satuanHargaJualTextView.setText(detailDataProduk.getSatuanHargaJual().getNama());
        }
    }

    @Override
    public void updateProdukPenyakit(List<ProdukPenyakit> produkPenyakitList) {
        StringBuilder namaPenyakit = new StringBuilder();
        for (int i=0; i<produkPenyakitList.size(); i++) {
            namaPenyakit.append(produkPenyakitList.get(i).getNamaPenyakit());
            if(i<produkPenyakitList.size()-1) {
                namaPenyakit.append(", ");
            }
        }
        penyakitTextView.setText(namaPenyakit);
    }

    @Override
    public void updateProdukPupuk(List<ProdukPupuk> produkPupukList) {
        StringBuilder namaPupuk = new StringBuilder();
        for (int i=0; i<produkPupukList.size(); i++) {
            namaPupuk.append(produkPupukList.get(i).getNamaPupuk());
            if(i<produkPupukList.size()-1) {
                namaPupuk.append(", ");
            }
        }
        pupukTextView.setText(namaPupuk);
    }


    @Override
    public void updateProdukFoto(List<ProdukFoto> produkFotoList) {
        mPhotoProdukAdapter.addItems(produkFotoList);
    }

    @Override
    public void updateProdukUserVote(List<ProdukUserVote> produkUserVotes) {
        jumlahVoteTextView.setText(String.valueOf(produkUserVotes.size()));
    }

    @Override
    public void updateProdukComment(List<ProdukComment> produkCommentList) {
        mKomentarProdukAdapter.addItems(produkCommentList);
    }


    @Override
    public void navigateDoneDeleteActivity() {
        getBaseActivity().onFragmentDetached(TAG);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_detail_produk, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Drawable drawable = item.getIcon();
        if (drawable instanceof Animatable) {
            ((Animatable) drawable).start();
        }
        switch (item.getItemId()) {
            case android.R.id.home:
                getBaseActivity().onBackPressed();
                return true;
            case R.id.action_delete_produk:
                mPresenter.deleteProduk();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onDestroyView() {
        mPresenter.onDetach();
        super.onDestroyView();
    }
}
