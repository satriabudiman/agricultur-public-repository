package org.agriculture.gateway.configuration;

import org.agriculture.gateway.model.User;
import org.agriculture.gateway.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
public class UserController {
    @Autowired
    private UserService service;

    private List<User> users = new ArrayList<>();

    @RequestMapping("/user")
    public Object user(Authentication authentication) {
        return authentication.getPrincipal();
    }

//    @GetMapping("/user")
//    @ResponseBody
//    public User getUser(@RequestParam String username) {
//        return service.get(username);
//    }
//
//    @PostMapping(value = "/user", consumes = MediaType.APPLICATION_JSON_VALUE)
//    @ResponseStatus(HttpStatus.CREATED)
//    public void postMessage(@RequestBody User user) {
//        users.add(user);
//    }
}
