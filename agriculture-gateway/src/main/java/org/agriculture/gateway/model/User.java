package org.agriculture.gateway.model;

import javax.persistence.*;

@Entity
@Table(name = "pengguna")
@SequenceGenerator(name = "user_id", sequenceName = "pengguna_id_seq")
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "user_id")
    private Integer id;

    @Column
    private String email;

    @Column(name = "nama")
    private String name;

    public User() {
    }

    public User(String email, String name) {
        super();
        this.id = id;
        this.email = email;
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
