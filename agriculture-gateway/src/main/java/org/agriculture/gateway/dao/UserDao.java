package org.agriculture.gateway.dao;

import org.agriculture.gateway.model.User;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;



@Repository
public class UserDao {

    @Autowired
    @Qualifier("sessionFactory")
    SessionFactory sessionFactory;

    public User get(String username) {
        Session session = this.sessionFactory.getCurrentSession();
        Query query = session.createQuery("FROM User U WHERE U.name=:username");
        query.setParameter("username", username);
        return (User) query.list().get(0);
    }
}
