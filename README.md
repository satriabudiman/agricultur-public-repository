# agriculture-public-repository
Bogor Agricultural University

# About
Data sharing produk hasil pertanian, berbasis Android.

Hanya pengguna user yang terverifikasi yang bisa mengupload data.

Misal peneliti/petani menanam suatu produk, maka setelah panen/gagal bisa membagikan hasilnya.

#### Data: 
- Nama Produk
- Status Berhasil/Gagal Panen
- Tanggal Tanam
- Tanggal Panen
- Luas Lahan
- Lokasi
- Iklim
- Harga Jual
- Satuan Harga Jual
- Harga beli bibit
- Harga jual
- Pupuk yang dipakai dan harganya
- Penyakit yang timbul pada produk
- Foto Produk

Pengguna publik juga dapat memberi respon terhadap data yang diupload:
- komentar
- vote apakah data baik/benar

# Android
Proyek ini dikembangkan dengan basis [Sumber](https://github.com/MindorksOpenSource/android-mvp-architecture)

#### Arsitektur:
- MVP 

#### Design Pattern:
- Singleton
- Depedency Injection
- ORM

#### Framework:
- Dagger2 (Depedency Injection)
- GreenDao (ORM)
- RxJava + FastAndroidNetworking
- ButterKnife


# Backend

#### Arsitektur:
- MVC


#### Design Pattern
- Singleton
- ORM
- Mediator

#### Framework:
- Spring
- Hibernate

# Database
- PostgreSQL
