package com.agriculture.backend.service;

import com.agriculture.backend.dao.PriceUnitSeedDao;
import com.agriculture.backend.model.PriceUnitSeed;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class PriceUnitSeedService {
    @Autowired
    PriceUnitSeedDao dao;

    @Transactional
    public List<PriceUnitSeed> getAll() {
        return dao.getAll();
    }

    @Transactional
    public void add(PriceUnitSeed seed) {
        dao.add(seed);
    }

}
