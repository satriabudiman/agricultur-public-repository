package com.agriculture.backend.service;

import com.agriculture.backend.dao.CategoryDao;
import com.agriculture.backend.model.AgriculturalProduct;
import com.agriculture.backend.model.Category;
import com.agriculture.backend.model.CategorySerializer;
import com.agriculture.backend.model.TemplateSerializer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service("categoryService")
public class CategoryService {

    @Autowired
    private CategoryDao dao;

    @Transactional
    public TemplateSerializer getAll() {
        return dao.getAll();
    }

    @Transactional
    public TemplateSerializer getAll(Category category) {
        return dao.getAll(category);
    }


}
