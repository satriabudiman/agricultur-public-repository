package com.agriculture.backend.service;

import com.agriculture.backend.dao.AgriculturalProductDao;
import com.agriculture.backend.model.AgriculturalProduct;
import com.agriculture.backend.model.Category;
import com.agriculture.backend.model.TemplateSerializer;
import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service("agriculturalProductService")
public class AgriculturalProductService {

    @Autowired
    AgriculturalProductDao dao;


    @Transactional
    public TemplateSerializer get (AgriculturalProduct agriculturalProduct) {
        return dao.get(agriculturalProduct);
    }

    @Transactional
    public Boolean add (AgriculturalProduct agriculturalProduct) {
        return dao.add(agriculturalProduct);
    }

    @Transactional
    public void update (AgriculturalProduct agriculturalProduct) {
        dao.update(agriculturalProduct);
    }
}
