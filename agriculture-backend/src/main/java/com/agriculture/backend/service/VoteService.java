package com.agriculture.backend.service;

import com.agriculture.backend.dao.VoteDao;
import com.agriculture.backend.model.Vote;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service("voteService")
public class VoteService {

    @Autowired
    VoteDao dao;

    @Transactional
    public Vote get(Vote vote) {
        return dao.get(vote);
    }

    @Transactional
    public Boolean add(Vote vote) {
        return dao.add(vote);
    }
}
