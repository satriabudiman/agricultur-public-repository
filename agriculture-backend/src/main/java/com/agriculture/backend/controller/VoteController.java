package com.agriculture.backend.controller;

import com.agriculture.backend.model.Vote;
import com.agriculture.backend.service.VoteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/vote")
public class VoteController {
    @Autowired
    VoteService service;

    @PostMapping(value = "/detail")
    public Vote find(@RequestBody Vote vote) {
        return service.get(vote);
    }

    @PostMapping(value = "")
    public ResponseEntity create (@RequestBody Vote vote) {
        if(service.add(vote)){
            return new ResponseEntity(HttpStatus.CREATED);
        }else {
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
    }
}
