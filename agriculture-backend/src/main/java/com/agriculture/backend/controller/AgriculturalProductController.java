package com.agriculture.backend.controller;

import com.agriculture.backend.model.AgriculturalProduct;
import com.agriculture.backend.model.Category;
import com.agriculture.backend.model.ResponseInitilizer;
import com.agriculture.backend.model.TemplateSerializer;
import com.agriculture.backend.service.AgriculturalProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/agriculturalproduct")
public class AgriculturalProductController {

    @Autowired
    private AgriculturalProductService service;

    @PostMapping(value = "/detail")
    public TemplateSerializer find (@RequestBody AgriculturalProduct agriculturalProduct) {
        return service.get(agriculturalProduct);
    }

    @PostMapping(value = "")
    public ResponseEntity create (@RequestBody AgriculturalProduct agriculturalProduct) {
        if(service.add(agriculturalProduct)) {
            ResponseInitilizer response = new ResponseInitilizer("success", "Success");
            return new ResponseEntity<ResponseInitilizer>(response, HttpStatus.CREATED);
        }else {
            ResponseInitilizer response = new ResponseInitilizer("failed", "Failed");
            return new ResponseEntity<ResponseInitilizer>(response, HttpStatus.BAD_REQUEST);
        }

    }

    @PutMapping(value = "")
    public void edit (@RequestBody AgriculturalProduct agriculturalProduct) {
        service.update(agriculturalProduct);
    }
}
