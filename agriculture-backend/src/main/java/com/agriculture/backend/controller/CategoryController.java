package com.agriculture.backend.controller;

import com.agriculture.backend.model.AgriculturalProduct;
import com.agriculture.backend.model.Category;
import com.agriculture.backend.model.CategorySerializer;
import com.agriculture.backend.model.TemplateSerializer;
import com.agriculture.backend.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/category")
public class CategoryController {

    @Autowired
    private CategoryService service;

    @GetMapping(value = "/summary")
    public TemplateSerializer list() {
        return service.getAll();
    }

    @PostMapping(value = "/product")
    public TemplateSerializer categoryDetail (@RequestBody Category category) {
        TemplateSerializer agriculturalProducts = service.getAll(category);
        return agriculturalProducts;
    }
}
