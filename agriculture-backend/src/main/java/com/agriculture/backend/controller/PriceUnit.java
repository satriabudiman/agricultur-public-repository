package com.agriculture.backend.controller;

import com.agriculture.backend.model.PriceUnitSeed;
import com.agriculture.backend.service.PriceUnitSeedService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/priceunit")
public class PriceUnit {
    @Autowired
    PriceUnitSeedService service;

    @GetMapping(value = "")
    public List<PriceUnitSeed> list() {
        return service.getAll();
    }

    @PostMapping(value = "")
    public void create(@RequestBody PriceUnitSeed seed) {
        service.add(seed);
    }
}
