package com.agriculture.backend.dao;

import com.agriculture.backend.model.AgriculturalProduct;
import com.agriculture.backend.model.Category;
import com.agriculture.backend.model.TemplateSerializer;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.PersistenceException;
import java.sql.SQLException;
import java.util.List;

@Repository
public class AgriculturalProductDao {

    @Autowired
    SessionFactory sessionFactory;

    public TemplateSerializer getAll (Category category) {
        Session session = this.sessionFactory.getCurrentSession();
        Query query = session.createQuery("FROM AgriculturalProduct AP WHERE AP.category.id=:category_id");
        query.setParameter("category_id", category.getId());
        List<AgriculturalProduct> agriculturalProducts = query.list();
        TemplateSerializer object = new TemplateSerializer(agriculturalProducts);
        return object;
    }

    public TemplateSerializer get(AgriculturalProduct agriculturalProduct) {
        Session session = this.sessionFactory.getCurrentSession();
        Query query = session.createQuery("FROM AgriculturalProduct AP WHERE AP.id=:id");
        query.setParameter("id", agriculturalProduct.getId());
        List<AgriculturalProduct> product = query.list();
        TemplateSerializer object = new TemplateSerializer(product.get(0));
        return object;
    }

    public Boolean add (AgriculturalProduct agriculturalProduct) {
        Session session = this.sessionFactory.getCurrentSession();
        try {
            session.persist(agriculturalProduct);
            return true;
        } catch (Exception e){
            return false;
        }
    }

    public void update (AgriculturalProduct agriculturalProduct) {
        Session session = this.sessionFactory.getCurrentSession();
        session.update(agriculturalProduct);
    }
}
