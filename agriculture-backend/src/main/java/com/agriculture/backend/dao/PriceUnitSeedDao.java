package com.agriculture.backend.dao;

import com.agriculture.backend.model.PriceUnitSeed;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class PriceUnitSeedDao {
    @Autowired
    SessionFactory sessionFactory;

    public List<PriceUnitSeed> getAll() {
        Session session = this.sessionFactory.getCurrentSession();
        return session.createQuery("FROM PriceUnitSeed").list();
    }

    public void add(PriceUnitSeed seed) {
        Session session = this.sessionFactory.getCurrentSession();
        session.persist(seed);
    }
}
