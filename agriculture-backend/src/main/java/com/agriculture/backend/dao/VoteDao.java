package com.agriculture.backend.dao;

import com.agriculture.backend.model.Vote;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.PersistenceException;
import java.util.List;

@Repository
public class VoteDao {

    @Autowired
    SessionFactory sessionFactory;

    public Vote get(Vote vote){
        Session session = this.sessionFactory.getCurrentSession();
        Integer id = vote.getId();
        return session.load(Vote.class, id);
    }

    public List<Vote> get(Integer id){
        Session session = this.sessionFactory.getCurrentSession();
        Query query = session.createQuery("FROM Vote V WHERE V.product.id=:id");
        query.setParameter("id", id);
        return query.list();
    }

    public Boolean add(Vote vote) {
        Session session = this.sessionFactory.getCurrentSession();
        List<Vote> list = get(vote.getProduct().getId());
        if(list.contains(vote)) return false;
        try{
            vote.setVote(true);
            session.persist(vote);
            return true;
        }catch (PersistenceException e){
            return false;
        }
    }
}
