package com.agriculture.backend.dao;

import com.agriculture.backend.model.AgriculturalProduct;
import com.agriculture.backend.model.Category;
import com.agriculture.backend.model.CategorySerializer;
import com.agriculture.backend.model.TemplateSerializer;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

@Repository
public class CategoryDao {

    @Autowired
    SessionFactory sessionFactory;

//    public List<CategorySerializer> getAll() {
//        Session session = this.sessionFactory.getCurrentSession();
//        ObjectMapper objectMapper = new ObjectMapper();
//        Criteria criteria = session.createCriteria(Category.class, "CS");
//        ProjectionList projectionList = Projections.projectionList();
//
//        criteria.createAlias("CS.product", "P");
//
//        projectionList.add(Projections.groupProperty("CS.name"))
//                .add(Projections.groupProperty("P.status"))
//                .add(Projections.alias(Projections.count("P.id"), "CS.jumlah_data"));
//
//        criteria.setProjection(projectionList);
//        List<CategorySerializer> category = criteria.list();
//
//        return category;
//    }

    public TemplateSerializer getAll() {
        Session session = this.sessionFactory.getCurrentSession();
        Query query = session.createQuery("FROM Category C");
//        Query query = session.createQuery("SELECT C.id, C.name, COUNT(P.status) FROM Category C JOIN C.product P GROUP BY C.id, C.name, P.status");
        List<Category> categories = query.list();
        List<CategorySerializer> categorySerializers = new ArrayList<CategorySerializer>() {};
        for (Category c: categories
             ) {
            Double success=0.0;
            CategorySerializer categorySerializer= new CategorySerializer();
            categorySerializer.setId(c.getId());
            categorySerializer.setName(c.getName());
            categorySerializer.setJumlah_data(c.getProduct().size());
            for (AgriculturalProduct ap : c.getProduct()
                 ) {
                if(ap.getStatus().equals("t"))success++;
            }
            categorySerializer.setPersen_berhasil(success/categorySerializer.getJumlah_data());

            categorySerializers.add(categorySerializer);
        }
        TemplateSerializer object = new TemplateSerializer(categorySerializers);
        return object;
    }

    public TemplateSerializer getAll (Category category) {
        Session session = this.sessionFactory.getCurrentSession();
        Query query = session.createQuery("FROM AgriculturalProduct AP WHERE AP.category.id=:category_id");
        query.setParameter("category_id", category.getId());
        List<AgriculturalProduct> agriculturalProducts = query.list();
        TemplateSerializer object = new TemplateSerializer(agriculturalProducts);
        return object;
    }
}
