package com.agriculture.backend.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import org.hibernate.annotations.Proxy;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@Table(name = "jenis_produk")
@SequenceGenerator(name = "category_id", sequenceName = "jenis_produk_id_seq")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
@Proxy(lazy = false)
public class Category{

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "category_id")
    private Integer id;

    @Column(name = "nama")
    private String name;

    @OneToMany(mappedBy = "category", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<AgriculturalProduct> product;

//    private Integer jumlah_data;
//    private String status_panen;


    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

//    public void setJumlah_data(Integer jumlah_data) {
//        this.jumlah_data = jumlah_data;
//    }
//
//    public Integer getJumlah_data() {
//        return jumlah_data;
//    }
//
//    public void setStatus_panen(String status_panen) {
//        this.status_panen = status_panen;
//    }
//
//    public String getStatus_panen() {
//        return status_panen;
//    }

    public void setProduct(List<AgriculturalProduct> product) {
        this.product = product;
    }

    public List<AgriculturalProduct> getProduct() {
        return product;
    }
}
