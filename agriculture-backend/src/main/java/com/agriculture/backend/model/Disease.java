package com.agriculture.backend.model;

import org.hibernate.annotations.Proxy;

import javax.persistence.*;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "jenis_penyakit")
@SequenceGenerator(name = "disease_id", sequenceName = "jenis_penyakit_id_seq")
@Proxy(lazy = false)
public class Disease {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "disease_id")
    private Integer id;

    @Column(name = "nama")
    private String name;

    @ManyToMany(mappedBy = "diseases", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private Set<AgriculturalProduct> agriculturalProducts = new HashSet<>();

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setAgriculturalProducts(Set<AgriculturalProduct> agriculturalProducts) {
        this.agriculturalProducts = agriculturalProducts;
    }

    public Set<AgriculturalProduct> getAgriculturalProducts() {
        return agriculturalProducts;
    }
}
