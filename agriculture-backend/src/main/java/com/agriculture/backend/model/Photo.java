package com.agriculture.backend.model;

import org.hibernate.annotations.Proxy;

import javax.persistence.*;
import java.sql.Date;

@Entity
@Table(name = "foto_hasil_pertanian")
@SequenceGenerator(name = "photo_id", sequenceName = "foto_hasil_pertanian_id_seq")
@Proxy(lazy = false)
public class Photo {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "photo_id")
    private Integer id;

    @Column(name = "foto")
    private String url;

    @Column(name = "tanggal_unggah")
    private Date created_at;

    @ManyToOne
    @JoinColumn(name = "id_data", nullable = false)
    AgriculturalProduct product;

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUrl() {
        return url;
    }

    public void setCreated_at(Date created_at) {
        this.created_at = created_at;
    }

    public Date getCreated_at() {
        return created_at;
    }

    public void setProduct(AgriculturalProduct product) {
        this.product = product;
    }

    public AgriculturalProduct getProduct() {
        return product;
    }

}
