package com.agriculture.backend.model;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "satuan_harga_bibit")
@SequenceGenerator(name = "priceUnitSeed_id", sequenceName = "satuan_harga_bibit_id_seq")
public class PriceUnitSeed {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "priceUnitSeed_id")
    private Integer id;

    @Column(name = "nama_satuan")
    private String name;

    @OneToMany(mappedBy = "satuanHargaModal")
    private List<AgriculturalProduct> product;

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<AgriculturalProduct> getProduct() {
        return product;
    }

    public void setProduct(List<AgriculturalProduct> product) {
        this.product = product;
    }
}
