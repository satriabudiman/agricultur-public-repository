package com.agriculture.backend.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import org.hibernate.annotations.Proxy;
import org.springframework.boot.autoconfigure.domain.EntityScan;

import javax.persistence.*;
import java.sql.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "data_hasil_pertanian")
@SequenceGenerator(name = "data_id", sequenceName = "data_hasil_pertanian_id_seq", initialValue = 1, allocationSize = 100)
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
@Proxy(lazy = false)
public class AgriculturalProduct {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "data_id")
    private Integer id;

    @Column(name = "status_panen")
    private Boolean status;

    @Column(name = "tanggal_tanam")
    private Date tanggalTanam;

    @Column(name = "tanggal_panen")
    private Date tanggalPanen;

    @Column(name = "luas_lahan")
    private Double luasLahan;

    @Column
    private String lokasi;

    @Column
    private Double suhu;

    @Column
    private Double modal;

    @Column
    private Double harga;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "product")
    private Set<Photo> photos;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "product")
    private List<Comment> comments;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_jenis_produk")
    private Category category;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "penyakit_hasil_pertanian",
            joinColumns = { @JoinColumn(name = "id_data") },
            inverseJoinColumns = { @JoinColumn(name = "id_penyakit")}
    )
    Set<Disease> diseases = new HashSet<>();

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "pupuk",
            joinColumns = { @JoinColumn(name = "id_data") },
            inverseJoinColumns = { @JoinColumn(name = "id_pupuk") }
    )
    Set<FertilizerCategory> pupuk = new HashSet<>();

    @ManyToOne
    @JoinColumn(name = "id_satuan_harga_bibit")
    private PriceUnitSeed satuanHargaModal;

    @ManyToOne
    @JoinColumn(name = "id_satuan_harga_jual")
    private PriceUnitSell satuanHargaJual;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "product")
    private Set<Vote> votes = new HashSet<>();

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setTanggalPanen(Date tanggalPanen) {
        this.tanggalPanen = tanggalPanen;
    }

    public Date getTanggalPanen() {
        return tanggalPanen;
    }

    public void setTanggalTanam(Date tanggalTanam) {
        this.tanggalTanam = tanggalTanam;
    }

    public Date getTanggalTanam() {
        return tanggalTanam;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setLuasLahan(Double luasLahan) {
        this.luasLahan = luasLahan;
    }

    public Double getLuasLahan() {
        return luasLahan;
    }

    public void setLokasi(String lokasi) {
        this.lokasi = lokasi;
    }

    public String getLokasi() {
        return lokasi;
    }

    public void setSuhu(Double suhu) {
        this.suhu = suhu;
    }

    public Double getSuhu() {
        return suhu;
    }


    public void setModal(Double modal) {
        this.modal = modal;
    }

    public Double getModal() {
        return modal;
    }

    public void setHarga(Double harga) {
        this.harga = harga;
    }

    public Double getHarga() {
        return harga;
    }

    public void setPhotos(Set<Photo> photos) {
        this.photos = photos;
    }

    public Set<Photo> getPhotos() {
        return photos;
    }

    public void setComments(List<Comment> comments) {
        this.comments = comments;
    }

    public List<Comment> getComments() {
        return comments;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public Category getCategory() {
        return category;
    }

    public void setDiseases(Set<Disease> diseases) {
        this.diseases = diseases;
    }

    public Set<Disease> getDiseases() {
        return diseases;
    }

    public PriceUnitSeed getSatuanHargaModal() {
        return satuanHargaModal;
    }

    public void setSatuanHargaJual(PriceUnitSell satuanHargaJual) {
        this.satuanHargaJual = satuanHargaJual;
    }

    public PriceUnitSell getSatuanHargaJual() {
        return satuanHargaJual;
    }

    public void setSatuanHargaModal(PriceUnitSeed satuanHargaModal) {
        this.satuanHargaModal = satuanHargaModal;
    }

    public Set<Vote> getVotes() {
        return votes;
    }

    public void setVotes(Set<Vote> votes) {
        this.votes = votes;
    }

    public Set<FertilizerCategory> getPupuk() {
        return pupuk;
    }

    public void setPupuk(Set<FertilizerCategory> pupuk) {
        this.pupuk = pupuk;
    }
}
