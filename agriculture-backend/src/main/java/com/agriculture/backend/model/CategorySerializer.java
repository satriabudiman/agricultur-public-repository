package com.agriculture.backend.model;

import java.io.Serializable;

public class CategorySerializer implements Serializable{
    private Integer id;
    private String name;
    private Integer jumlah_data;
    private Double persen_berhasil;

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setPersen_berhasil(Double persen_berhasil) {
        this.persen_berhasil = persen_berhasil;
    }

    public Double getPersen_berhasil() {
        return persen_berhasil;
    }

    public void setJumlah_data(Integer jumlah_data) {
        this.jumlah_data = jumlah_data;
    }

    public Integer getJumlah_data() {
        return jumlah_data;
    }

}
