package com.agriculture.backend.model;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "satuan_harga_jual")
@SequenceGenerator(name = "priceUnitSell", sequenceName = "satuan_harga_jual_id_seq")
public class PriceUnitSell {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "priceUnitSell")
    private Integer id;

    @Column(name = "nama_satuan")
    private String name;

    @OneToMany(mappedBy = "satuanHargaJual")
    private List<AgriculturalProduct> products;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<AgriculturalProduct> getProducts() {
        return products;
    }

    public void setProducts(List<AgriculturalProduct> products) {
        this.products = products;
    }
}
