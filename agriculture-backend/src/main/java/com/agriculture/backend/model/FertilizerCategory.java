package com.agriculture.backend.model;

import javax.persistence.*;

@Entity
@Table(name = "produk_pupuk")
@SequenceGenerator(name = "fertilizerCategory_id", sequenceName = "produk_pupuk_id_seq")
public class FertilizerCategory {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "fertilizerCategory_id")
    private Integer id;

    @Column(name = "nama_pupuk")
    private String name;

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
