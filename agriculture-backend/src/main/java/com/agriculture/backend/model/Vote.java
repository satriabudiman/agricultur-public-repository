package com.agriculture.backend.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.sun.org.apache.xpath.internal.operations.Bool;
import org.hibernate.annotations.Proxy;

import javax.persistence.*;

@Entity
@Table(name = "votes")
@SequenceGenerator(name = "vote_id", sequenceName = "votes_id_seq")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
@Proxy(lazy = false)
public class Vote {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "vote_id")
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "id_data")
    private AgriculturalProduct product;

    @ManyToOne
    @JoinColumn(name = "id_pengguna")
    private User user;

    @Column
    private Boolean vote;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public AgriculturalProduct getProduct() {
        return product;
    }

    public void setProduct(AgriculturalProduct product) {
        this.product = product;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Boolean getVote() {
        return vote;
    }

    public void setVote(Boolean vote) {
        this.vote = vote;
    }

}
