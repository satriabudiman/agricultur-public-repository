package com.agriculture.backend.model;

import org.hibernate.annotations.Proxy;

import javax.persistence.*;
import java.sql.Date;

@Entity
@Table(name = "komentar")
@SequenceGenerator(name = "comment_id", sequenceName = "komentar_id_seq")
@Proxy(lazy = false)
public class Comment {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "comment_id")
    private Integer id;

    @Column(name = "komentar")
    private String comment;

    @Column(name = "tanggal_komentar")
    private Date created_at;

    @ManyToOne
    @JoinColumn(name = "id_data", nullable = false)
    private AgriculturalProduct product;

    @ManyToOne
    @JoinColumn(name = "id_pengguna")
    private User user;

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getComment() {
        return comment;
    }

    public void setProduct(AgriculturalProduct product) {
        this.product = product;
    }

    public AgriculturalProduct getProduct() {
        return product;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public User getUser() {
        return user;
    }

    public void setCreated_at(Date created_at) {
        this.created_at = created_at;
    }

    public Date getCreated_at() {
        return created_at;
    }
}
