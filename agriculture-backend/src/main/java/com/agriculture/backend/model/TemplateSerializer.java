package com.agriculture.backend.model;

public class TemplateSerializer {
    private Object data;

    public TemplateSerializer(Object data) {
        this.data = data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public Object getData() {
        return data;
    }
}
