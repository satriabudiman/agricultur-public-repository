/**
 * Kelas ini akan mencek token end point dari pengguna sebelum pengguna dapat mengakses
 * resource. Cara kerjanya dengan menggunakan remote token service
 */
package com.agriculture.backend.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;
//import org.springframework.security.config.annotation.web.builders.HttpSecurity;
//import org.springframework.security.config.http.SessionCreationPolicy;
//import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
//import org.springframework.security.oauth2.provider.token.RemoteTokenServices;

//public class Oauth2ResourceServerConfigRemoteTokenService extends ResourceServerConfigurerAdapter {
//    @Override
//    public void configure(final HttpSecurity http) throws Exception {
//        // @formatter:off
//        http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.IF_REQUIRED)
//                .and()
//                .authorizeRequests().anyRequest().permitAll();
//        // @formatter:on
//    }
//
//    @Primary
//    @Bean
//    public RemoteTokenServices tokenServices() {
//        final RemoteTokenServices tokenService = new RemoteTokenServices();
//        tokenService.setCheckTokenEndpointUrl("http://localhost:8082/api/v1/oauth/check_token");
//        tokenService.setClientId("clientIdPassword");
//        tokenService.setClientSecret("secret");
//        return tokenService;
//    }
//}

public class Oauth2ResourceServerConfigRemoteTokenService {}
