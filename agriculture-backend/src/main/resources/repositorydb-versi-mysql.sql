-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Dec 18, 2017 at 03:21 pm
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `agriculturerepo`
--

-- --------------------------------------------------------

--
-- Table structure for table `data_hasil_pertanian`
--

CREATE TABLE IF NOT EXISTS `data_hasil_pertanian` (
  `id` int(11) NOT NULL,
  `id_jenis_produk` int(11) NOT NULL,
  `tanggal_tanam` date DEFAULT NULL,
  `tanggal_panen` date DEFAULT NULL,
  `luas_lahan` double DEFAULT NULL,
  `lokasi` text,
  `suhu` double DEFAULT NULL,
  `harga` double DEFAULT NULL,
  `id_pengguna` int(11) DEFAULT NULL,
  `status_panen` tinyint(1) NOT NULL,
  `modal` double DEFAULT NULL,
  `id_penyakit` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `detail_pengguna`
--

CREATE TABLE IF NOT EXISTS `detail_pengguna` (
  `id_pengguna` int(11) DEFAULT NULL,
  `institusi` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `foto_hasil_pertanian`
--

CREATE TABLE IF NOT EXISTS `foto_hasil_pertanian` (
  `id` int(11) NOT NULL,
  `id_data` int(11) NOT NULL,
  `foto` text,
  `tanggal_unggah` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `jenis_penyakit`
--

CREATE TABLE IF NOT EXISTS `jenis_penyakit` (
  `id` int(11) NOT NULL,
  `nama` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `jenis_produk`
--

CREATE TABLE IF NOT EXISTS `jenis_produk` (
  `id` int(11) NOT NULL,
  `nama` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `kandidat_donatur_data`
--

CREATE TABLE IF NOT EXISTS `kandidat_donatur_data` (
  `id_pengguna` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `komentar`
--

CREATE TABLE IF NOT EXISTS `komentar` (
  `id` int(11) NOT NULL,
  `id_data` int(11) NOT NULL,
  `id_pengguna` int(11) NOT NULL,
  `komentar` text,
  `tanggal_komentar` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pengguna`
--

CREATE TABLE IF NOT EXISTS `pengguna` (
  `id` int(11) NOT NULL,
  `nama` text NOT NULL,
  `email` varchar(100) DEFAULT NULL,
  `password` varchar(100) NOT NULL,
  `id_role` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `penyakit_hasil_pertanian`
--

CREATE TABLE IF NOT EXISTS `penyakit_hasil_pertanian` (
  `id_data` int(11) DEFAULT NULL,
  `id_penyakit` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE IF NOT EXISTS `roles` (
  `id` int(11) NOT NULL,
  `role` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `votes`
--

CREATE TABLE IF NOT EXISTS `votes` (
  `id` int(11) NOT NULL,
  `id_data` int(11) NOT NULL,
  `id_pengguna` int(11) NOT NULL,
  `vote` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `data_hasil_pertanian`
--
ALTER TABLE `data_hasil_pertanian`
 ADD PRIMARY KEY (`id`), ADD KEY `id_jenis_produk` (`id_jenis_produk`);

--
-- Indexes for table `detail_pengguna`
--
ALTER TABLE `detail_pengguna`
 ADD KEY `id_pengguna` (`id_pengguna`);

--
-- Indexes for table `foto_hasil_pertanian`
--
ALTER TABLE `foto_hasil_pertanian`
 ADD PRIMARY KEY (`id`), ADD KEY `id_data` (`id_data`);

--
-- Indexes for table `jenis_penyakit`
--
ALTER TABLE `jenis_penyakit`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jenis_produk`
--
ALTER TABLE `jenis_produk`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kandidat_donatur_data`
--
ALTER TABLE `kandidat_donatur_data`
 ADD KEY `id_pengguna` (`id_pengguna`);

--
-- Indexes for table `komentar`
--
ALTER TABLE `komentar`
 ADD PRIMARY KEY (`id`), ADD KEY `id_data` (`id_data`), ADD KEY `id_pengguna` (`id_pengguna`);

--
-- Indexes for table `pengguna`
--
ALTER TABLE `pengguna`
 ADD PRIMARY KEY (`id`), ADD KEY `id_role` (`id_role`);

--
-- Indexes for table `penyakit_hasil_pertanian`
--
ALTER TABLE `penyakit_hasil_pertanian`
 ADD KEY `id_data` (`id_data`), ADD KEY `id_penyakit` (`id_penyakit`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `votes`
--
ALTER TABLE `votes`
 ADD PRIMARY KEY (`id`), ADD KEY `id_data` (`id_data`), ADD KEY `id_pengguna` (`id_pengguna`), ADD KEY `id_data_2` (`id_data`);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `data_hasil_pertanian`
--
ALTER TABLE `data_hasil_pertanian`
ADD CONSTRAINT `data_hasil_fk_id_jenis_produk` FOREIGN KEY (`id_jenis_produk`) REFERENCES `jenis_produk` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `detail_pengguna`
--
ALTER TABLE `detail_pengguna`
ADD CONSTRAINT `detail_pengguna_fk_id_pengguna` FOREIGN KEY (`id_pengguna`) REFERENCES `pengguna` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `foto_hasil_pertanian`
--
ALTER TABLE `foto_hasil_pertanian`
ADD CONSTRAINT `foto_hasil_pertanian_fk_id_data` FOREIGN KEY (`id_data`) REFERENCES `data_hasil_pertanian` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `kandidat_donatur_data`
--
ALTER TABLE `kandidat_donatur_data`
ADD CONSTRAINT `kandidat_donatur_data_fk_id_pengguna` FOREIGN KEY (`id_pengguna`) REFERENCES `pengguna` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `komentar`
--
ALTER TABLE `komentar`
ADD CONSTRAINT `komentar_fk_id_data` FOREIGN KEY (`id_data`) REFERENCES `data_hasil_pertanian` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `komentar_fk_id_pengguna` FOREIGN KEY (`id_pengguna`) REFERENCES `pengguna` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `pengguna`
--
ALTER TABLE `pengguna`
ADD CONSTRAINT `pengguna_fk_id_role` FOREIGN KEY (`id_role`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `penyakit_hasil_pertanian`
--
ALTER TABLE `penyakit_hasil_pertanian`
ADD CONSTRAINT `penyakit_hasil_pertanian_fk_id_data` FOREIGN KEY (`id_data`) REFERENCES `data_hasil_pertanian` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `penyakit_hasil_pertanian_fk_id_penyakit` FOREIGN KEY (`id_penyakit`) REFERENCES `jenis_penyakit` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `votes`
--
ALTER TABLE `votes`
ADD CONSTRAINT `votes_fk_id_data` FOREIGN KEY (`id_data`) REFERENCES `data_hasil_pertanian` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `votes_fk_id_pengguna` FOREIGN KEY (`id_pengguna`) REFERENCES `pengguna` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
