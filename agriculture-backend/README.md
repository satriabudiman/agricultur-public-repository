# Agriculture_Public_Repository
### Gateway
#### Login [POST]
```
:8082/api/v1/oauth/token
```
Header request parameter
```
"content-type": "application/x-www-form-urlencoded",
"authorization": "Basic clientIdPassword:secret"
```
Body request parameter 
```
"username": "mandan",
"password": "secret",
"client_id": "clientIdPassword",
"grant_type": "password"
```
### Agriculture data
#### GET
get list all data
```
:8081/api/v1/agriculturalproduct
```
get detail data produk
```
:8081/api/v1/agriculturalproduct/{id}
```
#### POST
```
:8081/api/v1/agriculturalproduct
```
parameter
```
        {
            "status": true,
            "tanggalTanam": "2015-12-01",
            "tanggalPanen": "2016-01-31",
            "luasLahan": 340,
            "lokasi": "Jl. Sancang Dalam nomor 32",
            "suhu": 28,
            "modal": 500000,
            "harga": 5000,
            "photos": [
                {
                    "url": "http://programhcs.com/wp-content/uploads/2016/12/Aplikasi-Suplemen-Organik-Tanaman-Untuk-Padi.jpg",
                	"created_at": "2018-01-08"
                }
            ],
            "category": {
                "name": "Kentang"
            },
            "diseases": [
            	{
	            	"name": "Tomcat"
	            }	
            ]
        }
```
#### PUT
```
:8081/api/v1/agriculturalproduct
```
parameter
```
"id": 1,
"status": "berhasil/gagal",
"tanggalTanam": "12-12-2017",
"tanggalPanen": "12-12-2017",
"luasLahan": 340,
"lokasi": "Sancang Dalam nomor 5",
"suhu": 28.0,
"penyakit": "tomcat",
"modal": 500000.00, 
"harga": 5000.00
```

### Category
#### get list nama produk [GET]
```
:8081/api/v1/category/summary
```
#### get list data produk berdasar id nama produk (nama produk yg sama) [POST]
```
:8081/api/v1/category/product
```
Parameter
```
{
    "id": 1,
    "name": "Padi"
}
```
Response
```
{
    "data": [
        {
            "id": 4,
            "status": false,
            "tanggalTanam": "2016-10-01",
            "tanggalPanen": "2016-12-26",
            "luasLahan": 960,
            "lokasi": "Jl. Bima Nomor 1",
            "suhu": 29,
            "modal": 1000000,
            "harga": 10000,
            "photos": [
                {
                    "id": 5,
                    "url": "http://programhcs.com/wp-content/uploads/2016/12/Aplikasi-Suplemen-Organik-Tanaman-Untuk-Padi.jpg",
                    "created_at": "2018-01-03",
                    "product": 4
                }
            ],
            "comments": [],
            "category": {
                "id": 1,
                "name": "Padi",
                "product": [
                    4
                ]
            },
            "diseases": [
                {
                    "id": 2,
                    "name": "Tomcat",
                    "agriculturalProducts": [
                        4
                    ]
                }
            ]
        }
    ]
}
```
