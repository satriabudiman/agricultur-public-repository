<?php 

$uri_path = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
$uri_segments = explode('/', $uri_path);

$target = $uri_segments[3];
if(isset($uri_segments[4])) {
	$target .= "/".$uri_segments[4];
}

$json = '';
switch($target) {
	case '588d14f4100000a9072d2943': 
		include "login_google.php";
		$json=$login_google;
	break;
	
	case '588d15d3100000ae072d2944': 
		include "login_facebook.php";
		$json=$login_facebook;
	break;
	
	case 'oauth/token': 
		include "login_server.php";
		$json=$login_server;
	break;
	
	case '588d161c100000a9072d2946': 
		include "logout.php";
		$json=$logout;
	break;
	
	case '5926ce9d11000096006ccb30': 
		include "blog.php";
		$json=$blog;
	break;
	
	case '5926c34212000035026871cd': 
		include "open_source.php";
		$json=$open_source;
	break;
	
	case 'category/summary': 
		include "data_list_nama_hasil_pertanian.php";
		$json=$data_list_nama_hasil_pertanian;
	break;	
	
	case 'category/product': 
		include "data_list_data_hasil_pertanian.php";
		$json=$data_list_data_hasil_pertanian;
	break;
	
	case 'agriculturalproduct/detail': 
		include "data_list_detail_data_hasil_pertanian.php";
		$json=$data_list_detail_data_hasil_pertanian;
	break;
	
	case 'agriculturalproduct':
		include "add_data_produk.php";
		$json=$add_data_produk;
	break;
	
	case '5926c34212000035026871ci':
		include "delete_data_produk.php";
		$json=$delete_data_produk;
	break;
}

echo $json;

?>
